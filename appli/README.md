_English below_ 


**FR** |


## Organisation des dossiers

Vous trouverez l'application dans le dossier **APK**. 

Pour celles et ceux qui souhaitent modifier l'application ou tout simplement voir comment elle est réalisée, vous trouverez le code source dans le dossier **Code** 

## Installation de l'application

Vous trouverez l'application dans le dossier **APK**. Une fois téléchargé, il vous suffira ensuite de cliquer sur le fichier .APK pour que l'application s'installe sur votre téléphone. Il est très probable que votre téléphone vous affiche des alertes de sécurités, néanmoins vous ne courrez aucun risque donc vous pouvez cliquer sur installer quand même. En fonction de l'endroit sur lequel vous vous trouvez (dossier de téléchargement, messagerie, etc.), il est également possible que votre téléphone vous demande d'aller dans vos paramètres pour accepter le téléchargement des fichiers en .apk, dans ce cas-là faîtes le, vous pourrez toujours retourner dans vos paramètres pour empécher l'installation des fichiers en .apk ultérieurement. 

## Remarques

**Veillez à activer votre bluetooth et la géolocalisation lorsque vous utilisez l'application** sinon celle-ci ne fonctionnera pas. Les données de localisation ne sont à aucun moment utilisée par l'application mais sont nécessaires pour faire fonctionner le bluetooth. 

## Aide pour Android Studio

Installer [Node.js](https://nodejs.org/fr/) 
Installer Cordova en suivant la documentation sur https://cordova.apache.org/
Installer [Android Studio](https://developer.android.com/studio/)

### Windows
Pour créer un nouveau projet, ouvrir un invite de commande, vous placer avec la commande _cd_ à l'endroit où vous souhaitez créer le dossier qui contiendra votre projet puis entrer la commande suivante : 
```
cordova create <directory_name> // changer <directory_name> par le nom de votre projet
```

Cette commande va créer un dossier avec le nom indiqué dans <directory_name> que vous pouvez ouvrir avec votre exlorateur de fichiers.  

![Explorateur_de_fichier](./figs/explorateur_fichier_exofinger.JPG)

Dans ce dossier, remplacez les fichiers avec ceux que vous trouverez dans ce gitlab dans le dossier _Appli/Code_

Ensuite, dans l'invite de commande, entrez les commandes suivantes : 

```
cd <directory_name> // changer <directory_name> par le nom de votre projet
cordova platform add android
```

Vous pouvez ensuite vous rendre sur android Studio et ouvrir votre projet en sélectionnant le dossier Android de votre projet qui viendra tout juste d'être créé. 

Vous pourrez alors avoir accès aux fichiers _index.html_, _index.js_ et _index.css_ directement sur Android Studio et les modifier à votre guise. 

**ATTENTION : Il se peut lorsque vous allez faire tourner votre application que vous obteniez le message d'erreur suivant _Could not reserve enough space for 2097152KB object heap_, pour prévenir ce problème, allez dans le dossier _platforms > android > Cordova > lib > config_, ouvrez le fichier GradlePropertiesParser.js_ et remplacer la ligne `'org.gradle.jvmargs': '-Xmx2048m'` par `'org.gradle.jvmargs': '-Xmx512m'`**


### Visualisation de l'application

Pour visualiser en direct les changements que vous effectuez sur l'application, il y a deux possibilités
    
        - Utiliser un émulateur de téléphone
        - Faire tourner l'application sur votre téléphone connecté par cable USB à votre ordinateur 

Pour configurer un émulateur, il faut vous rendre sur _tools > AVD Manager > Create Virtual Device_. Vous trouverez des tutoriels facilement sur internet si vous avez des difficultés à le configurer. 

Pour faire tourner l'application sur votre téléphone, il vous suffit de connecter par cable USB votre téléphne à votre ordinateur. Puis dans les _paramètres_ de votre téléphone allez dans le dossier _A propos du téléphone > Informations sur le logiciel_ et cliquez 7 fois sur _Numéro de Version_. Retournez sur vos _paramètres_, un nouveau dossier appelé _Options de développement_ est normalement apparu. Dans ce dossier vous devez activer la fonction _débogage USB_. Si ces instructions ne fonctionnent pas pour votre téléphone, il existe également des tutoriels sur internet accessibles facilement pour activer le débogage USB. 

Que ce soit avec un émulateur ou directement sur votre téléphone, vous devrez ensuite cliquer sur _run app_ dans la barre d'outil en haut à droite ou dans le menu _run_

### Génération d'un fichier .apk

Pour générer un fichier en .APK pour pouvoir installer l'application sur votre téléphone et la partager, vous devez vous rendre dans Android Studio dans _Build > Generate Signed Bundle/APK ..._ . Sélectionner ensuite _APK_, _create new_, complétez les informations demandées :

![signed Apk information](./figs/signed_apk.JPG)

 cliquez sur _next_, puis sélectionnez _release_ puis cliquez sur _Finish_. Si tout se passe bien vous devriez obtenir le message suivant en bas à droite. 

## Licence

* Le code source est sous licence [Cecill](../COPYING)

**EN** | 

## Folder organization 

You will find the application in the **APK** folder. Then you just need to click on the. APK file to install the app on your phone. It is very likely that your phone will show you security alerts, nevertheless you will not run any risk so you can click install anyway. Depending on where you are located (download folder, messaging, etc.), it is also possible that your phone will ask you to go to your settings in order to accept the download of files with the .apk extension. In that case do it. You can always go back to your settings to prevent the installation of files in .apk later.

For those who wish to modify the application or simply see how it is made, you will find the source code in the **Code** folder

## Remarks

**Be sure to activate your bluetooth and geolocation when using the application** otherwise it will not work. Location data is not used by the app at any time but is necessary to operate bluetooth.

## Help for Android Studio

Install [Node.js](https://nodejs.org/fr/) 
Install Cordova by following the documentation on https://cordova.apache.org/
Install [Android Studio](https://developer.android.com/studio/)

### Windows
To create a new project, open a command prompt, place yourself with the _cd_ command where you want to create the folder that will contain your project, and then enter the following command:

```
cordova create <directory_name> // change <directory_name> to the name of your project
```
This command will create a folder with the name indicated in <directory_name>. Then you can open this folder with your file exlorator.  

![Explorateur_de_fichier](./figs/explorateur_fichier_exofinger.JPG)

In this folder, replace the files with those you will find in this gitlab in the _Appli/Code_ folder

Then, in the command prompt, enter the following commands:

```
cd <directory_name> // change <directory_name> to the name of your projct
cordova platform add android
```

You can then go to android Studio and open your project by selecting the Android folder of your project that has just been created.

You will then be able to access the _index.html_, _index.js_ and _index.css_ files directly on Android Studio and edit them as you wish.

**CAUTION: It is possible that when you are going to run your application you get the following error message _Could not reserve enough space for 2097152KB object heap_, to prevent this problem, go to the _platforms > android folder > Cordova > lib > config_, open the GradlePropertiesParser file.js_ and replace the line ''org.gradle.jvmargs': '-Xmx2048m'' with ''org.gradle.jvmargs': '-Xmx512m''**

### Viewing of the application

To view live the changes you make on the app, there are two possibilities:
    
	- Use a phone emulator 
	- Run the application on your phone connected by USB cable to your computer

To set up an emulator, you must go to _tools > AVD Manager > Create Virtual Device_. You will find tutorials easily on the internet if you have difficulty configuring it.

To run the app on your phone, simply connect your phone to your computer via USB cable. Then in the _settings_ of your phone go to _About phone > Software Information_ and click _Build number_ 7 times. Going back to your _settings_, a new folder called _Developper options_ has normally appeared. In this folder you need to enable the _USB debugging_ function. If these instructions don't work for your phone, there are also easily accessible tutorials on the internet to enable USB debugging.

Whether you use an emulator or directly your phone, you will then have to click on _run app_ in the toolbar at the top right or in the _run_ menu.

### Generation of a .apk file

To generate a file in. APK in order to install the app on your phone and share it, you need to go to Android Studio in _Build > Generate Signed Bundle/APK..._. Then select _APK_, _create new_, and complete the requested information:
![signed Apk information](./figs/signed_apk.JPG)





![signed Apk success message](./figs/buildapk.JPG)

Vous pouvez cliquer sur _locate_, cela ouvrira le dossier dans lequel se trouve votre .apk

## License

* The code source is under [Cecill](../COPYING) license
