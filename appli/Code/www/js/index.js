'use strict';

// ASCII only
function bytesToString(buffer)
{
    return String.fromCharCode.apply(null, new Uint8Array(buffer));
}

// ASCII only
function stringToBytes(string)
{
    var array = new Uint8Array(string.length);

    for(var i = 0, l = string.length; i < l; i++)
    {
        array[i] = string.charCodeAt(i);
    }

    return array.buffer;
}

var exofinger =
{
    serviceUUID: '6e400001-b5a3-f393-e0a9-e50e24dcca9e',
    txCharacteristic: '6e400002-b5a3-f393-e0a9-e50e24dcca9e', // transmit is from the phone's perspective
    rxCharacteristic: '6e400003-b5a3-f393-e0a9-e50e24dcca9e', // receive is from the phone's perspective
    motorRatio: 2500, // 6250 (big ratio) or 2500 steps for 1 cm
};

var app =
{

    initialize: function()
    {
        setTimeout(() => {  $("#index").remove()}, 2000);
        document.addEventListener("deviceReady", app.onDeviceReady, false);
        $("#refreshButton").on("click", app.disconnect);
        $("#refreshButton").on("click", app.refreshDeviceList);
        $("#openButton").on("click", app.open);
        $("#closeButton").on("click", app.close);
        $("#openButtondebug").on("click", app.opendebug);
        $("#closeButtondebug").on("click", app.closedebug);
        $("#clearButtondebug").on("click", app.cleardebug);
        $("#courseButton").on("click", app.setCourse);
        $("#disconnectButton").on("click", app.disconnect);
        $("#course").on("input", app.updateCourse);
        $("#motorRatioChoice").on("input", app.motorRatioChoice);
    },

    onDeviceReady: function()
    {
        app.refreshDeviceList();
    },

    refreshDeviceList: function()
    {
        $("#refreshButton").addClass("btnclick");
        $("#deviceList").html(""); // empties the list
        ble.scan([exofinger.serviceUUID], 5, app.onDiscoverDevice, app.onError);
    },

    onDiscoverDevice: function(device)
    {
        if(device.name == "ExoFinger")
        {
            var listItem = $("<li/>").data("deviceId", device.id)
                                     .html(device.name + "&nbsp;" +
                                        "(RSSI: " + device.rssi + "dBm&nbsp;|&nbsp;" +
                                        device.id + ")")
                                     .on("click", app.connect);
            $("#deviceList").append(listItem);
        }
    },

    connect: function(e)
    {
        var target = $(e.target)
        var deviceId = $(e.target).data("deviceId")

        target.addClass("connection")

        function onConnect(peripheral)
        {
            app.determineWriteType(peripheral);


            // subscribe for incoming data
            ble.startNotification(deviceId, exofinger.serviceUUID, exofinger.rxCharacteristic, app.onData, app.onError);

            $("#disconnectButton").data("deviceId", deviceId);
            target.removeClass("connection");
            target.addClass("connected");
            app.sendData("I");

            $("#courseButton").removeClass("btnhighlight");
            document.getElementById("course").value= "4";
            $("#courseDisplay").html(document.getElementById("course").value + "cm")
            $("#position").html(" ");
            $("#consignedebug").html( "± " + 4 + " cm");
            $("#positiondebug").html(" ");
        };

        // If not already connected, connect to the selected device
        if(!$("#disconnectButton").data("deviceId"))
        {
            ble.connect(deviceId, onConnect, app.onError);
        }
    },

    determineWriteType: function(peripheral)
    {
        var characteristic = peripheral.characteristics.filter(function(element)
        {
            if(element.characteristic.toLowerCase() === exofinger.txCharacteristic)
            {
                return element;
            }
        })[0];

        app.writeWithoutResponse = (characteristic.properties.indexOf('WriteWithoutResponse') > -1);
    },

    onData: function(data)
    {
        var buffer = new Uint8Array(data);

        var pwm = (buffer[2] << 8) + buffer[3];
        var course = (buffer[4] << 24) + (buffer[5] << 16) + (buffer[6] << 8) + buffer[7];
        var position = (buffer[8] << 24) + (buffer[9] << 16) + (buffer[10] << 8) + buffer[11];
		var batterylevel = (((buffer[12] << 8) + buffer[13]) - 3200)/10;

		if(batterylevel>100)
        {
            batterylevel = 100;
        }
        if(batterylevel<0)
        {
            batterylevel = 0;
        }

        $("#pwm").html(pwm);
        $("#position").html(" " + position/2500 + " cm");
        $("#positiondebug").html(" " + position/2500 + " cm");
        $("#courseapplied").html(" +/- " + course/2500 + " cm");
        $("#battery").addClass("FadeIn");
        $("#batterylevel").html(" " + parseInt(batterylevel) + "%");

        if(batterylevel<30)
            {
            $("#batterylevel").addClass("blink");
            document.getElementById('iconbattery').src="img/iconbatteryred.png"
            }
        else
            {
            $("#batterylevel").removeClass("blink");
            document.getElementById('iconbattery').src="img/iconbattery.png"
            }
    },

    sendData: function(data)
    {
        var deviceId = $("#disconnectButton").data("deviceId");

        function success()
        {
        };

        function failure(reason)
        {
            alert("Failed writing data to ExoFinger " + JSON.stringify(reason));
        };

        if(deviceId)
        {
            if(app.writeWithoutResponse)
            {
                ble.writeWithoutResponse(
                    deviceId,
                    exofinger.serviceUUID,
                    exofinger.txCharacteristic,
                    stringToBytes(data), success, failure
                );
            }
            else
            {
                ble.write(
                    deviceId,
                    exofinger.serviceUUID,
                    exofinger.txCharacteristic,
                    stringToBytes(data), success, failure
                );
            }
        }
    },

    open: function(event)
    {
        $("#openButton").addClass("btnclick");
        app.sendData("O");
    },

    close: function(event)
    {
        $("#closeButton").addClass("btnclick");
        app.sendData("C");
    },

    opendebug: function(event)
    {
        $("#openButtondebug").addClass("btnclick");
        app.sendData("N");
    },

    closedebug: function(event)
    {
        $("#closeButtondebug").addClass("btnclick");
        app.sendData("E");
    },

    cleardebug: function(event)
    {
        $("#clearButtondebug").addClass("btnclick");
        app.sendData("R");
    },

    setCourse: function(event)
    {
        app.sendData("L" + parseFloat($("#courseDisplay").html()))
        $("#courseButton").addClass("btnhighlight");
        var value =  $("#courseDisplay").data("courseDisplay");
        if (value == null)
        {value=4;}
        $("#consignedebug").html( "± " + value + "  cm");
    },

    disconnect: function(event)
    {
        $("#disconnectButton").addClass("btnclick");
        $("#battery").removeClass("FadeIn");
        $("#batterylevel").removeClass("blink");

        $("#courseButtondebug").removeClass("btnhighlight");
        document.getElementById("course").value= "4";
        $("#courseDisplay").html(4+ " cm")
        $("#position").html(" ");
        $("#consignedebug").html(" ");
        $("#positiondebug").html(" ");

        var deviceId = $("#disconnectButton").data("deviceId");

        if(deviceId)
        {
            ble.disconnect(deviceId, app.disconnected, app.onError);
        }
    },

    disconnected: function()
    {
        $("#deviceList > li").removeClass("connected");
        $("#disconnectButton").data("deviceId", null);
    },

    onError: function(reason)
    {
        alert("ERROR: " + JSON.stringify(reason)); // real apps should use notification.alert
    },

    updateCourse: function()
    {   $("#courseButton").removeClass("btnhighlight");
        $("#courseDisplay").html($(this).val() + " cm");
        $("#courseDisplay").data("courseDisplay", $(this).val());
    },

    motorRatioChoice: function(event)
    {
        if ($("#motorRatioChoice").prop("checked")) {
            exofinger.motorRatio = 6250;
        }
        else {
            exofinger.motorRatio = 2500;
        }
    }
};

app.initialize();
