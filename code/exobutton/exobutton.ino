/*
* This file is a part of Exofinger
*
* Copyright (C) 2022 Inria
*
* This software is governed by the CeCILL license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 
*
* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability.
*/

#include <bluefruit.h>

BLEDis bledis;
BLEHidAdafruit blehid;

#define BUTTON_PIN A0

void setup() 
{
  Serial.begin(115200);

  Bluefruit.begin(1, 1);
  Bluefruit.setTxPower(4);    // Check bluefruit.h for supported values
  Bluefruit.setName("ExoButton");

  // Configure and Start Device Information Service
  bledis.setManufacturer("ExoFinger");
  bledis.setModel("ExoButton BBBB");
  bledis.begin();

  blehid.begin();

  // Pin Setup
  pinMode(BUTTON_PIN, INPUT_PULLUP);
  pinMode(LED_BLUE, INPUT);

  // Set up and start advertising
  startAdv();
}

void startAdv(void)
{  
  // Advertising packet
  Bluefruit.Advertising.addFlags(BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE);
  Bluefruit.Advertising.addTxPower();
  Bluefruit.Advertising.addAppearance(BLE_APPEARANCE_HID_KEYBOARD);
  
  // Include BLE HID service
  Bluefruit.Advertising.addService(blehid);

  // There is enough room for the dev name in the advertising packet
  Bluefruit.Advertising.addName();
  
  Bluefruit.Advertising.restartOnDisconnect(true);
  Bluefruit.Advertising.setInterval(32, 244);    // in unit of 0.625 ms
  Bluefruit.Advertising.setFastTimeout(30);      // number of seconds in fast mode
  Bluefruit.Advertising.start(0);                // 0 = Don't stop advertising after n seconds
}

void loop() 
{
  static char last_state = 0;
  char button_state = !digitalRead(BUTTON_PIN); 

  // Testing
  Serial.printf("READ %d\n",button_state);

  if(button_state != last_state)
  {
    if(button_state)
    {
      blehid.keyPress('A');
    }
    else
    {
      blehid.keyRelease();
    }
  }

  last_state = button_state;
  delay(100);
}
