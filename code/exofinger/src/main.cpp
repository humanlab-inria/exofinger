/*
* This file is a part of Exofinger
*
* Copyright (C) 2022 Inria
*
* This software is governed by the CeCILL license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 
*
* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability.
*/

// Version modifiée avec la carte fille PCB Exofinger V3

//Modif par arthur

#include <bluefruit.h>  // Dans le gestionnaire de carte, il faut utiliser la version board 'Adafruit nrf52' 0.21.0. et non pas la plus récente.
#include "RotaryEncoder.h"  // librairie de l'encodeur

#define CM (-2500) // Number of ticks per centimeter (may change if you use 1:500 motor and not 1:200 motor)

#define DEFAULT_POS_CLOSED  (6 * CM)

#define THRES_ERROR (10)
#define P           (0.3)
#define I           (0.5)
#define DT          (0.01)
#define ALPHA       (0.7)
#define INT_MAX     (1000.0)

#define ENC_C1      A4//A0
#define ENC_C2      A5//A1 
#define ENC_VCC     26//aussi nommé SCK sur carte//13 

#define VBATPIN     A6

#define MOT_IN1      6//11 
#define MOT_IN2      9//12 

//#define CM          A3// Proportionnel au courant moteur
#define nSHDN       12
#define nFault      11
#define nSleep      10

#define MIN_PWM     85 // 85 / 512 -> 16.6%


// To connect a phone or PC using UART interface
BLEUart bleuart;


bool closed = false;
bool arrived = true;
int consigne = DEFAULT_POS_CLOSED;
int target = 0;
char output_buf[14];
int lenght_avg = 500; // Default value that can be changed
float list_value[500];
bool clear_ = false;
bool debug_ = false;
int coursedebug = DEFAULT_POS_CLOSED;
float measuredvbat = 0;


void enable_encoder(void)
{
  digitalWrite(ENC_VCC, 0);
}

void disable_encoder(void)
{
  digitalWrite(ENC_VCC, 1);  
}

void start_adv(void)
{
  // Advertising packet
  Bluefruit.Advertising.addFlags(BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE);
  Bluefruit.Advertising.addTxPower();
 
  // Include bleuart 128-bit uuid
  Bluefruit.Advertising.addService(bleuart);
 
  // Secondary Scan Response packet (optional)
  // Since there is no room for 'Name' in Advertising packet
  Bluefruit.ScanResponse.addName();
 
  Bluefruit.Advertising.restartOnDisconnect(true);
  Bluefruit.Advertising.setInterval(32, 244);    // in unit of 0.625 ms
  Bluefruit.Advertising.setFastTimeout(30);      // number of seconds in fast mode
  Bluefruit.Advertising.start(0);                // 0 = Don't stop advertising after n seconds
}

void scan_callback(ble_gap_evt_adv_report_t* report)
{
  // Since we configure the scanner with filterUuid()
  // Scan callback only invoked for device with hid service advertised  
  // Connect to the device with hid service in advertising packet
  Bluefruit.Central.connect(report);
}

void prph_connect_callback(uint16_t conn_handle)
{
  (void) conn_handle;
  Serial.println("CONNECTED !");
}

void prph_disconnect_callback(uint16_t conn_handle, uint8_t reason)
{
  (void) conn_handle;
  (void) reason;
}


void disconnect_callback(uint16_t conn_handle, uint8_t reason)
{
  (void) conn_handle;
  (void) reason;
}

float moving_avg(int current_value){ 
  
  double sum = 0;
  for (int i=1; i<=(lenght_avg-1); i++){
    list_value[i-1] = list_value[i];
    sum += list_value[i-1];  
  }
  list_value[lenght_avg-1] = current_value;
  sum += list_value[lenght_avg-1];
  return(sum/lenght_avg);
}


void uart_rx_callback(uint16_t dummy)
{
  // Forward data from Mobile to our peripheral
  char str[21] = { 0 };
  int value = 0;
  bleuart.read(str, sizeof(str) - 1);
  Serial.print("RECEIVED COMMAND : ");
  Serial.println(str);

  switch(str[0])
  {
    case 'O': //open
      closed = false;
      consigne = 0 ;
      debug_ = false;
      break;

    case 'C': //close
      closed = true;
      consigne = DEFAULT_POS_CLOSED;
      debug_ = false;
      break;

    case 'L': // set course value
      value = (int)(atof(str + 1) * CM);
      coursedebug = value;      
      break;

    case 'N': // Open debug
      closed = false;
      debug_ = true;
      consigne -= coursedebug; 
      break;

    case 'E': // close debug
      closed = true;
      debug_ = true;
      consigne += coursedebug; 
      break;

    case 'I': // Initialisation
    closed = false;
    arrived = true;
    consigne = 0;
    debug_ = false;
    coursedebug = DEFAULT_POS_CLOSED;
    value = 0;
    for (int i=0; i<=(lenght_avg-1); i++)
    {
      list_value[i] = measuredvbat; 
    }
    break;

    case 'R': // position set to 0
    clear_ = true;
    break;
    

    default:
    Serial.printf("Unknown command: %s\n", str);
    break;
  }
}

uint32_t last_debug_time;

void setup()
{
  Serial.begin(115200);

  // Initialize Bluefruit with maximum connections as Peripheral = 1, Central = 1
  Bluefruit.begin(1, 1);
  
  Bluefruit.setName("ExoFinger");

  // Initialize BLE UART service
  bleuart.begin();
  bleuart.setRxCallback(uart_rx_callback);

  Bluefruit.Periph.setConnectCallback(prph_connect_callback);
  Bluefruit.Periph.setDisconnectCallback(prph_disconnect_callback);


  // Increase Blink rate to different from PrPh advertising mode
  Bluefruit.setConnLedInterval(250);


  /* Start Central Scanning
   * - Enable auto scan if disconnected
   * - Interval = 100 ms, window = 80 ms
   * - Don't use active scan
   * - Filter only accept HID service in advertising
   * - Start(timeout) with timeout = 0 will scan forever (until connected)
   */
  Bluefruit.Scanner.setRxCallback(scan_callback);
  Bluefruit.Scanner.restartOnDisconnect(true);
  Bluefruit.Scanner.setInterval(160, 80); // in unit of 0.625 ms
  Bluefruit.Scanner.useActiveScan(false);
  Bluefruit.Scanner.start(0);             // 0 = Don't stop scanning after n seconds

  // Start advertising
  start_adv();

  // Pin Setup
  pinMode(LED_BLUE, INPUT); // Deactivate blue LED to reduce power consumption
  pinMode(ENC_VCC, OUTPUT); // Motor encoder
  
  pinMode(MOT_IN1, OUTPUT);
  digitalWrite(MOT_IN1, 0);
  pinMode(MOT_IN2, OUTPUT);
  digitalWrite(MOT_IN2, 0);

  pinMode(nSHDN, OUTPUT);
  digitalWrite(nSHDN, 1);

  pinMode(nSleep, OUTPUT);
  digitalWrite(nSleep, 1);

  // Encoder
  enable_encoder();
  RotaryEncoder.begin(ENC_C1 , ENC_C2 );
  RotaryEncoder.start();

  // PWM
  HwPWM0.addPin(MOT_IN1);
  HwPWM0.begin();
  HwPWM0.setResolution(9); // 9-bit resolution (max is 15 bits)
  HwPWM0.setClockDiv(PWM_PRESCALER_PRESCALER_DIV_1); // freq = 16Mhz => PWM frequency is 31.25kHz

  HwPWM1.addPin(MOT_IN2);
  HwPWM1.begin();
  HwPWM1.setResolution(9); // 9-bit resolution (max is 15 bits)
  HwPWM1.setClockDiv(PWM_PRESCALER_PRESCALER_DIV_64); // freq = 16Mhz => PWM frequency is 31.25kHz

  last_debug_time = millis();
}


void loop()
{
  static int position = 0;
  static int precedentposition = 0;
  static int deltapos = 0;
  static float int_error = 0.0;
  
  measuredvbat = analogRead(VBATPIN);
  measuredvbat *= 2;    // we divided by 2, so multiply back
  measuredvbat *= 3.6;  // Multiply by 3.6V, our reference voltage
  measuredvbat /= 1024; // convert to voltage
  measuredvbat *= 1000; // convert in mV
  int batterylevel =(int)moving_avg(measuredvbat); 

  // Update position with encoder reading

  if (clear_)
  {
    precedentposition = 0;
    position = 0;
    clear_ = false;
    consigne = 0;
  }
  else
  {
    precedentposition = position;
    position += RotaryEncoder.read();
  }
  deltapos = position - precedentposition;

  if(closed)
  {
    target = consigne;
  }
  else
  {
    // Opened
    if (debug_ == false)
        {target = 0;}
    else
    {target = consigne;}
  }

  // PI computation
  int error = target - position;

  // If error is very small, set it to zero to avoid oscillations
  if(abs(error) < THRES_ERROR)
  {
    error = 0;
  }
  
  float cmd = P * error + I * int_error;

  // Update error integral with dampening
  int_error = ALPHA * (error * DT) + (1 - ALPHA) * int_error;

  if(fabsf(int_error) > INT_MAX)
  {
    if(int_error > 0)
    {
      int_error = INT_MAX;
    }
    else
    {
      int_error = -INT_MAX;
    }
  }

  // // Set direction
  // if(cmd > 0)
  // {
  //   // Serial.println("DIRECTION SET TO 0");
  //   digitalWrite(MOT_PH, 0);
  // }
  // else
  // {
  //   // Serial.println("DIRECTION SET TO 1");
  //   digitalWrite(MOT_PH, 1);
  // }

  // Set motor "speed": 0 - 511
  int value = (int)fabsf(cmd);
  
  if(value > 511)
  {
    value = 511;
  }
  
  if(value < MIN_PWM || (deltapos < 10 && position > 100 && closed == true && debug_== 0)) 
  {
    arrived = true;
    //disable_encoder();
  }
  else
  {
    arrived = false;
    //enable_encoder();
  

    // Set direction and speed
      if(cmd > 0)
      {
        // Serial.println("DIRECTION SET TO 0");
        HwPWM0.writePin(MOT_IN1, value, false);
        HwPWM1.writePin(MOT_IN2, 0, false);
        
      }
      else
      {
        // Serial.println("DIRECTION SET TO 1");
        HwPWM0.writePin(MOT_IN1, 0, false);
        HwPWM1.writePin(MOT_IN2, value, false);
      }

  }
 
  if (millis() - last_debug_time > 500)
  {
    Serial.printf("dbg: %d %d %d %d\n", position, value, arrived, (cmd > 0) );
    Serial.print("ERROR : " + String(error));
    Serial.println("           INT_ERROR : " + String(int_error));
    last_debug_time = millis();
  }
  


  if(bleuart.notifyEnabled())
  {
    uint8_t cpt = 0;
    
    output_buf[cpt++] = closed;
    output_buf[cpt++] = arrived;
    output_buf[cpt++] = (value >> 8) & 0xFF;
    output_buf[cpt++] = value & 0xFF;
    output_buf[cpt++] = (consigne >> 24) & 0xFF;    
    output_buf[cpt++] = (consigne >> 16) & 0xFF;
    output_buf[cpt++] = (consigne >> 8) & 0xFF;
    output_buf[cpt++] = consigne & 0xFF;
    output_buf[cpt++] = (position >> 24) & 0xFF;    
    output_buf[cpt++] = (position >> 16) & 0xFF;
    output_buf[cpt++] = (position >> 8) & 0xFF;
    output_buf[cpt++] = position & 0xFF;
    output_buf[cpt++] = (batterylevel >> 8) & 0xFF;
    output_buf[cpt++] = batterylevel & 0xFF;
    
    bleuart.write(output_buf, sizeof(output_buf));
  }  

  delay((int)(DT * 1000));
}

