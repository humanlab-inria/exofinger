//test Encodeur

#include <bluefruit.h>  // Dans le gestionnaire de carte, il faut utiliser la version board 'Adafruit nrf52' 0.21.0. et non pas la plus récente.
#include "RotaryEncoder.h"  // librairie de l'encodeur

#define CM (2500) // Number of ticks per centimeter (may change if you use 1:500 motor and not 1:200 motor)

#define DEFAULT_POS_CLOSED  (4 * CM)

#define THRES_ERROR (10)
#define P           (1.0)
#define I           (1.0)
#define DT          (0.01)
#define ALPHA       (0.7)
#define INT_MAX     (1000.0)

#define ENC_C1      A4//A0
#define ENC_C2      A5//A1 
#define ENC_VCC     26//aussi nommé SCK sur carte//13 

#define VBATPIN     A6

#define MOT_IN1      6//11 
#define MOT_IN2      9//12 

//#define CM          A3// Proportionnel au courant moteur
#define nSHDN       12
#define nFault      11
#define nSleep      10

#define MIN_PWM     85 // 85 / 512 -> 16.6%



bool closed = false;
bool arrived = true;
int consigne = DEFAULT_POS_CLOSED;
int target = 0;
char output_buf[14];
int lenght_avg = 500; // Default value that can be changed
float list_value[500];
bool clear_ = false;
bool debug_ = false;
int coursedebug = DEFAULT_POS_CLOSED;
float measuredvbat = 0;


void enable_encoder(void)
{
  digitalWrite(ENC_VCC, 0);
}

void disable_encoder(void)
{
  digitalWrite(ENC_VCC, 1);  
}




void setup()
{
  Serial.begin(115200);

  

  // Pin Setup
  pinMode(LED_BLUE, INPUT); // Deactivate blue LED to reduce power consumption
  pinMode(ENC_VCC, OUTPUT); // Motor encoder
  
  // pinMode(MOT_IN1, OUTPUT);
  // digitalWrite(MOT_IN1, 0);
  // pinMode(MOT_IN2, OUTPUT);
  // digitalWrite(MOT_IN2, 0);

  pinMode(nSHDN, OUTPUT);
  digitalWrite(nSHDN, 1);

  pinMode(nSleep, OUTPUT);
  digitalWrite(nSleep, 1);

  // Encoder
  enable_encoder();
  RotaryEncoder.begin(ENC_C1, ENC_C2);
  RotaryEncoder.start();

  // PWM
  HwPWM0.addPin(MOT_IN1);
  HwPWM0.begin();
  HwPWM0.setResolution(9); // 9-bit resolution (max is 15 bits)
  HwPWM0.setClockDiv(PWM_PRESCALER_PRESCALER_DIV_64); // freq = 16Mhz => PWM frequency is 31.25kHz

  HwPWM1.addPin(MOT_IN2);
  HwPWM1.begin();
  HwPWM1.setResolution(9); // 9-bit resolution (max is 15 bits)
  HwPWM1.setClockDiv(PWM_PRESCALER_PRESCALER_DIV_64); // freq = 16Mhz => PWM frequency is 31.25kHz
}


void loop()
{  
      //Direction 0 Direction ouverture du gant
      HwPWM0.writePin(MOT_IN1, 125, false);
      HwPWM1.writePin(MOT_IN2, 0, false);
      delay(3000);

      //Direction 1 Direction  de fermeture du gant
      HwPWM0.writePin(MOT_IN1, 0, false);
      HwPWM1.writePin(MOT_IN2, 125, false);
      delay(3000);     
}

