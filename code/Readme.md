**FR** |

Vous trouverez dans les dossiers ci-dessus, les codes arduino à téléverser sur les cartes Adafruit. 

Pour utiliser ces codes, il vous faudra installer au préalable la **version 0.21.0** - important pour des raisons de compatibilité - de la **carte Adafruit nRF52** dans le **gestionnaire de carte** (Outils>Type de carte).  Veillez également à bien sélectionner le type de carte Adafruit Feather nRF52840 Express.

Pour la version 2.1, le code de la carte exofinger est compilé en utilisant la plateforme [platformio](https://platformio.org/). La carte bouton reste pour l'instant programmé en ide/arduino.

Le code source est sous licence [Cecill](../COPYING) 

**EN** |

You will find in the folders above, the arduino codes that you can upload into the Adafruit boards. To use these codes, you will first need to install **0.21.0 version** - important for compatibility reasons - of the **Adafruit nRF52 boards** in the **Boards Manager** (Tools > Boards:"" > Boards Manager). Also, be sure to select the Adafruit Feather nRF52840 Express Boards.

For version 2.1, the exofinger board code is compiled using the [platformio](https://platformio.org/) platform. The button board remains programmed in ide/arduino for the time being.

 The code is under [Cecill](../COPYING) license