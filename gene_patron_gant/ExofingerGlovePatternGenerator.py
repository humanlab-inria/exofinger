# This file is a part of Exofinger
#
# Copyright (C) 2021 Inria
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.

"""Exofinger made to measure glove pattern generator GUI 

Usage: python ExofingerGlovePatternGenerator.py [file.json]
The optional argument file.json allows to initialise the interface with specific values
for most of the parameters. If not provided, default values are set.

The file.json can be created by saving modified parameters inside the application.
Requirements: ExofingerGlovePattern.py (then matplotlib and beziers packages)

Auhtor: L. Boissieux (2021)
"""

# GUI module
import tkinter as tk
from tkinter import font
from tkinter import filedialog as fd
from tkinter import messagebox as mb
# "Drawing" module
import matplotlib.pyplot as plt
# GUI / Drawing integration
plt.matplotlib.use('tkAgg')
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
# Json config
import json
# to retreive command line arguments
import sys
# Computation / drawing script
from ExofingerGlovePattern import drawPattern, drawPatternV2

class ExofingerGlovePattern(tk.Tk):
    """constructor"""
    def __init__(self):
        tk.Tk.__init__(self)
        # Fields & values init     
        self.handEntries = []
        self.handValues = None
        self.patternEntries = []
        self.patternValues = None
        # Radio Buttons
        self.hand = tk.StringVar()
        self.lang = tk.StringVar()
        self.patternVersion = tk.StringVar()
        # CheckBox 
        self.drawGrid = tk.IntVar()
        self.drawHand = tk.IntVar()
        self.drawThumb = tk.IntVar()
        # Matplotlib plot and figure
        self.plt = None
        self.fig = None
        self.returnCode = ""
        # Layout and widgets 
        self.fMain = None
        self.fSettings = None
        self.lSettings = None
        self.lSettingsText = tk.StringVar()
        self.ibEnglish = None
        self.ibFrench = None
        self.bLoadParamText = tk.StringVar()
        self.bSaveParamText = tk.StringVar()
        self.bDefaultParamText = tk.StringVar()
        self.fMeasure = None
        self.lMeasure = None
        self.lMeasureText = tk.StringVar()
        self.ibMeasure = None   
        self.fHand = None
        self.lHand = None
        self.lHandText = tk.StringVar()
        self.rbHandRText = tk.StringVar()
        self.rbHandLText = tk.StringVar()
        self.fPattern = None
        self.lPattern = None
        self.lPatternText = tk.StringVar()
        self.cbThumbText = tk.StringVar()
        self.cbHandText = tk.StringVar()
        self.cbGridText = tk.StringVar()
        self.fCommands = None
        self.bDrawText = tk.StringVar()      
        self.bSavePGNText = tk.StringVar()
        self.bSavePDFText = tk.StringVar()
        self.bQuitText = tk.StringVar()
        self.fLogo = None
        self.fCanvas = None
        # Widgets 
        self.canvas = None
        self.iMeasure = []

        # Language translation
        self.labels = { 'fSettings' : ('Settings','Réglages'),
                        'fMeasure' : ('How to perform measurements','Comment prendre les mesures'),
                        'fHand' : ('Hand parameters','Paramètres de la main'),
                        'rbHandR' : ('Right','Droite'), 'rbHandL' : ('Left','Gauche'),
                        'fPattern' : ('Pattern parameters','Paramètres du patron'),
                        'cbThumb' : ('Thumb', 'Pouce'), 'cbHand' : ('Hand', 'Main'), 'gridCBox' : ('Grid', 'Grille'),
                        'Hand Contour' : ('Hand Contour','Tour Main'), 'Hand Back' : ('Hand Back','Dos Main'),'Hand Side' : ('Hand Side','Côté Main'),
                        'Hand Length' : ('Hand Length','Longueur Main'),
                        'Wrist Contour' : ('Wrist Contour','Tour Poignet'),'Wrist Back' : ('Wrist Back','Dos Poignet'),
                        'Thumb Contour' : ('Thumb Contour','Tour Pouce'), 'Thumb Length' : ('Thumb Length','Longueur Pouce'), 
                        'Thumb Proximal' : ('Thumb Proximal', 'Proximale Pouce'), 'Thumb Base' : ('Thumb Base', 'Base Pouce'), 
                        'Thumb Interior' : ('Thumb Interior','Interieur Pouce'),'Velcros Width' : ('Velcros Width','Largeur Velcros'),
                        'Text Offset' : ('Text Offset','Décalage Texte'),
                        'bDraw' : ('Draw','Tracer'), 'bLoadParam' : ('Load Param','Charger Param'), 'defaultParamButton' : ('Default','Par défaut'),
                        'bSaveParam' : ('Save Param','Sauver Param'), 'bSavePNG' : ('Save as PNG','Sauver en PNG'),'bSavePDF' : ('Save as PDF','Sauver en PDF'),
                        'bQuit' : ('Quit','Quitter')}
        
        # colors
        self.bkgColor = '#384257'
        self.textColor = '#fff'    

        if len(argv) > 1:
            self.setDefaultValues(argv[1])
        else:
            self.setDefaultValues("")
        self.createWidgets()
        self.changeLang() # to initialise labels
    

    def readParametersFromFile(self, fileName):
        with open(fileName, 'r') as f:
            fromJson = json.load(f)
        self.lang.set(fromJson[0]["Lang"])
        self.hand.set(fromJson[1]["Hand"])
        self.handValues = fromJson[2]
        self.patternVersion.set(fromJson[3]["Pattern Version"])
        self.patternValues = fromJson[4]
        self.drawGrid.set(int(fromJson[5]["Grid"]))

    def setDefaultValues(self,fileName=""):
        if fileName != "":
            self.readParametersFromFile(fileName)
        else : 
            self.handValues = { 'Hand Contour' : 18, 'Hand Back' : 9.5, 'Hand Side' : 7, 'Hand Length': 7.7,'Wrist Contour' : 17, 'Wrist Back' : 7.5, 
                    'Thumb Contour' : 6.0, 'Thumb Length' : 8, 'Thumb Proximal' : 3.5,'Thumb Base' : 5,
                   'Thumb Interior' : 8 }
            self.patternValues = { 'Velcros Width' : 2.0, 'Text Offset' : 0.2 }
            # Radio Buttons
            self.hand.set("Right")
            self.lang.set("French")
            self.patternVersion.set(1)
            # CheckBox 
            self.drawGrid.set(1)
        self.drawHand.set(1)
        self.drawThumb.set(1)

    def drawGridState(self):
        return (self.drawGrid.get() > 0)
        
    def drawHandState(self):
        return (self.drawHand.get() > 0)
        
    def drawThumbState(self):
        return (self.drawThumb.get() > 0)
        
    def getHand(self):
        self.hand.get()
        
    def getLangId(self):        
        if self.lang.get() == "English":
            return 0
        else:
            return 1      

    def getPatternVersion(self):
        return int(self.patternVersion.get())

    def fetchEntries(self, entries, values):
        try: 
            for e in entries:
                field = e[0]
                value = e[1].get()
                values[field] = float(value)
        except ValueError as err:
            mb.showerror("Error",str(err))

    def updateEntries(self,entries,values):
        for e in entries:
                field = e[0]
                # First remove all characters from entry value
                e[1].delete(0,tk.END)
                e[1].insert(tk.END, values[field])
            
    def createEntries(self,frame,row,entries,values):
        rowIndex = row
        lId = self.getLangId()
        i = 0
        for field in list(values.keys()):
            labVar = tk.StringVar()
            lab = tk.Label(frame,text=self.labels[field][lId], anchor='e', width=16,bg=self.bkgColor,fg=self.textColor,textvariable=labVar)
            lab.grid(row=rowIndex,column=0)
            entry = tk.Entry(frame, width=10)
            entry.insert(tk.END, values[field])
            entry.grid(row=rowIndex,column=1)           
            entries.append((field, entry, labVar))
            rowIndex += 1        
        return rowIndex

    def changeLang(self):
        lId = self.getLangId()
        self.lSettingsText.set(self.labels['fSettings'][lId])
        self.lMeasureText.set(self.labels['fMeasure'][lId])
        self.lHandText.set(self.labels['fHand'][lId])
        self.rbHandRText.set(self.labels['rbHandR'][lId])
        self.rbHandLText.set(self.labels['rbHandL'][lId])
        self.lPatternText.set(self.labels['fPattern'][lId])
        self.cbThumbText.set(self.labels['cbThumb'][lId])
        self.cbHandText.set(self.labels['cbHand'][lId])
        self.cbGridText.set(self.labels['gridCBox'][lId])
        for e in self.handEntries:
            field = e[0]
            e[2].set(self.labels[field][lId])            
        for e in self.patternEntries:
            field = e[0]
            e[2].set(self.labels[field][lId])
        self.bDrawText.set(self.labels['bDraw'][lId])
        self.bLoadParamText.set(self.labels['bLoadParam'][lId])
        self.bSaveParamText.set(self.labels['bSaveParam'][lId])
        self.bDefaultParamText.set(self.labels['defaultParamButton'][lId])
        self.bSavePGNText.set(self.labels['bSavePNG'][lId])
        self.bSavePDFText.set(self.labels['bSavePDF'][lId])
        self.bQuitText.set(self.labels['bQuit'][lId])
    
    
    def createWidgets(self):

        # Get default font param
        font = tk.font.nametofont('TkDefaultFont')
        fontFamily = font.cget("family")
        fontSize = font.cget("size")
        lId = self.getLangId()
        
        # Main Frame
        self.fMain = tk.Frame(self,bg=self.bkgColor)
        self.fMain.pack(side=tk.LEFT, expand=0)        

        # Global Settings
        self.fSettings = tk.Frame(self.fMain,bg=self.bkgColor)
        self.fSettings.pack(side=tk.TOP)
        self.lSettings = tk.Label(self.fSettings,text=self.labels['fSettings'][lId], textvariable = self.lSettingsText,anchor=tk.W,bg=self.bkgColor,fg=self.textColor,font=(fontFamily,fontSize + 2,'bold'))
        self.lSettings.grid(row=0,column=0) # separate .grid() cause it returns None, then it gives None type to widget var
        self.ibEnglish = tk.PhotoImage(file='en.png')
        self.rbEnglish = tk.Radiobutton(self.fSettings, image=self.ibEnglish, bg=self.bkgColor,fg=self.textColor,selectcolor=self.bkgColor,variable=self.lang, value="English",command=self.changeLang)
        self.rbEnglish.grid(row=1,column=1)
        self.ibFrench = tk.PhotoImage(file='fr.png')
        self.rbFrench = tk.Radiobutton(self.fSettings, image=self.ibFrench, bg=self.bkgColor,fg=self.textColor, selectcolor=self.bkgColor,variable=self.lang, value="French",command=self.changeLang)
        self.rbFrench.grid(row=1,column=2)
        self.bLoadParam = tk.Button(self.fSettings, text=self.labels['bLoadParam'][lId],textvariable = self.bLoadParamText,fg=self.bkgColor,command=self.loadParameters)
        self.bLoadParam.grid(row=2,column=0,pady=4, padx=5)
        self.bSaveParam = tk.Button(self.fSettings, text=self.labels['bSaveParam'][lId],textvariable = self.bSaveParamText,fg=self.bkgColor,command=self.saveParameters)
        self.bSaveParam.grid(row=2,column=1,pady=4, padx=5)
        self.bDefaultParam = tk.Button(self.fSettings, text=self.labels['defaultParamButton'][lId],textvariable = self.bDefaultParamText,fg=self.bkgColor,command=self.resetParameters)
        self.bDefaultParam.grid(row=2,column=2,pady=4, padx=5)
        
        # How to measure frame & button
        self.fMeasure = tk.Frame(self.fMain,bg=self.bkgColor)
        self.fMeasure.pack(side=tk.TOP)
        self.lMeasure = tk.Label(self.fMeasure,text=self.labels['fMeasure'][lId],  textvariable = self.lMeasureText,anchor=tk.W,bg=self.bkgColor,fg=self.textColor,font=(fontFamily,fontSize + 2,'bold'))
        self.lMeasure.pack(side=tk.TOP)
        self.ibMeasure = tk.PhotoImage(file='HowToMeasureButton.png')
        bMeasure = tk.Button(self.fMeasure, image=self.ibMeasure,command=self.showHowToMeasure,borderwidth=0,bg=self.bkgColor,fg=self.bkgColor)
        bMeasure.pack(pady=10)
        self.iMeasure.append(tk.PhotoImage(file='HowToMeasure_en.png'))
        self.iMeasure.append(tk.PhotoImage(file='HowToMeasure_fr.png'))
        
        # Hand frame & fields
        self.fHand = tk.Frame(self.fMain,bg=self.bkgColor)
        self.fHand.pack(side=tk.TOP, padx=10)        
        self.lHand = tk.Label(self.fHand,text=self.labels['fHand'][lId], textvariable = self.lHandText, width=17, anchor=tk.W,bg=self.bkgColor,fg=self.textColor,font=(fontFamily,fontSize + 2,'bold'))
        self.lHand.grid(row=0,column=0)
        self.lHand2 = tk.Label(self.fHand,text='(cm)', anchor=tk.W,bg=self.bkgColor,fg=self.textColor,font=(fontFamily,fontSize + 2,'bold'))
        self.lHand2.grid(row=0,column=1)
        self.rbHandR = tk.Radiobutton(self.fHand, text=self.labels['rbHandR'][lId], textvariable = self.rbHandRText, bg=self.bkgColor,fg=self.textColor,selectcolor=self.bkgColor,variable=self.hand, value="Right",command=self.getHand)
        self.rbHandR.grid(row=1,column=0)
        self.rbHandL = tk.Radiobutton(self.fHand, text=self.labels['rbHandL'][lId], textvariable = self.rbHandLText, bg=self.bkgColor,fg=self.textColor, selectcolor=self.bkgColor,variable=self.hand, value="Left",command=self.getHand)
        self.rbHandL.grid(row=1,column=1)
        r = self.createEntries(self.fHand, 2, self.handEntries, self.handValues)
        
        # Pattern frame & widgets
        self.fPattern = tk.Frame(self.fMain,bg=self.bkgColor)
        self.fPattern.pack(side=tk.TOP)
        self.lPattern = tk.Label(self.fPattern,text=self.labels['fPattern'][lId], textvariable = self.lPatternText,width=17,anchor=tk.W,bg=self.bkgColor,fg=self.textColor,font=(fontFamily,fontSize + 2,'bold'))
        self.lPattern.grid(row=0,column=0)
        self.rbV1 = tk.Radiobutton(self.fPattern, text='V1', bg=self.bkgColor,fg=self.textColor,selectcolor=self.bkgColor,variable=self.patternVersion, value=1,command=self.getPatternVersion)
        self.rbV1.grid(row=1,column=0)
        self.rbV2 = tk.Radiobutton(self.fPattern, text='V2', bg=self.bkgColor,fg=self.textColor, selectcolor=self.bkgColor,variable=self.patternVersion, value=2,command=self.getPatternVersion)
        self.rbV2.grid(row=1,column=1)
        self.cbDrawThumb = tk.Checkbutton(self.fPattern,anchor='w',bg=self.bkgColor,fg=self.textColor, selectcolor=self.bkgColor, text=self.labels['cbThumb'][lId],textvariable = self.cbThumbText, variable=self.drawThumb)
        self.cbDrawThumb.grid(row=2,column=0)
        self.cbDrawHand = tk.Checkbutton(self.fPattern,anchor='w',bg=self.bkgColor,fg=self.textColor, selectcolor=self.bkgColor, text=self.labels['cbHand'][lId], textvariable = self.cbHandText,variable=self.drawHand)
        self.cbDrawHand.grid(row=2,column=1)
        r = self.createEntries(self.fPattern, 3, self.patternEntries, self.patternValues)        
        self.cbDrawGrid = tk.Checkbutton(self.fPattern,anchor='w',bg=self.bkgColor,fg=self.textColor, selectcolor=self.bkgColor, text=self.labels['gridCBox'][lId], textvariable = self.cbGridText,variable=self.drawGrid)
        self.cbDrawGrid.grid(row=r,column=1)

        # Commands Frame
        self.fCommands = tk.Frame(self.fMain,bg=self.bkgColor)
        self.fCommands.pack(padx=10, pady=10)
        self.bDraw = tk.Button(self.fCommands, text=self.labels['bDraw'][lId],textvariable = self.bDrawText,fg=self.bkgColor, command=self.draw).grid(row=1,column=1,pady=4)
        self.bSavePNG = tk.Button(self.fCommands, text=self.labels['bSavePNG'][lId],textvariable = self.bSavePGNText,fg=self.bkgColor,command=self.savePNG).grid(row=2,column=0,pady=4)
        self.bSavePDF = tk.Button(self.fCommands, text=self.labels['bSavePDF'][lId],textvariable = self.bSavePDFText,fg=self.bkgColor,command=self.savePDF).grid(row=2,column=2,pady=4)
        self.bQuit = tk.Button(self.fCommands, text=self.labels['bQuit'][lId],textvariable = self.bQuitText,fg=self.bkgColor,command=self._quit).grid(row=3,column=1,pady=4)
        
        # Logo
        self.fLogo = tk.Frame(self.fMain,bg=self.bkgColor)
        self.fLogo.pack(side=tk.TOP)
        self.iLogo = tk.PhotoImage(file='HLIDark.png')
        lLabel= tk.Label(self.fLogo,image=self.iLogo,bg=self.bkgColor)       
        lLabel.pack()

        # Canvas
        self.fCanvas = tk.Frame(self,bg=self.bkgColor)
        self.fCanvas.pack()

    def applyParameters(self):
        # Force update of check boxes when new parameters come from reading a file or default values
        # and not from a click through the interface
        if self.lang.get() == "French":
            self.rbFrench.invoke()
        else:
            self.rbEnglish.invoke()    
        if self.hand.get() == "Right":
            self.rbHandR.invoke()
        else:
            self.rbHandL.invoke()
        if self.getPatternVersion() == 1:
            self.rbV1.invoke()
        else:
            self.rbV2.invoke()    
        # Display new values in entries fields
        self.updateEntries(self.handEntries, self.handValues)
        self.updateEntries(self.patternEntries, self.patternValues)

    def resetParameters(self):
        self.setDefaultValues()
        self.applyParameters()

    def loadParameters(self):
        # Open dialog box to get filename
        fileName = fd.askopenfilename(defaultextension=".json")
        self.readParametersFromFile(fileName)
        self.applyParameters()
        
    def saveParameters(self):
        # Ensure to retreive the last entered values
        self.fetchEntries(self.handEntries, self.handValues)
        self.fetchEntries(self.patternEntries, self.patternValues)
        fileName = fd.asksaveasfilename(defaultextension=".json")
        outJson = []
        outJson.append({"Lang" : self.lang.get()})
        outJson.append({"Hand" : self.hand.get()})
        outJson.append(self.handValues)
        outJson.append({"Pattern Version" : self.getPatternVersion()})
        outJson.append(self.patternValues)
        outJson.append({"Grid" : self.drawGrid.get()})
        with open(fileName, 'w') as f:
            json.dump(outJson, f, indent=2)

    def showHowToMeasure(self):
        # Open measurements image in a new window
        wMeasure = tk.Toplevel()
        wMeasure.title(self.labels['fMeasure'][self.getLangId()])
        cvMeasure = tk.Canvas(wMeasure, height=self.iMeasure[self.getLangId()].height(), width=self.iMeasure[self.getLangId()].width())
        cvMeasure.pack()
        cvMeasure.create_image(0,0,anchor=tk.NW, image=self.iMeasure[self.getLangId()])
        wMeasure.mainloop()
    
    def draw(self):
        # Remove fig and canvas if exist
        if self.fig != None:
            self.fig.clear()
            self.canvas.get_tk_widget().destroy()
        # Retrieve values from fields
        self.fetchEntries(self.handEntries, self.handValues)
        self.fetchEntries(self.patternEntries, self.patternValues)
        # Call pattern computation and drawing
        if self.getPatternVersion() == 1 :
            self.returnCode, self.plt, self.fig = drawPattern(self.handValues, self.patternValues, self.hand.get(), self.drawThumbState(), self.drawHandState(), self.drawGridState(), self.getLangId())
        else:
            self.returnCode, self.plt, self.fig = drawPatternV2(self.handValues, self.patternValues, self.hand.get(), self.drawThumbState(), self.drawHandState(), self.drawGridState(), self.getLangId())
        if (self.returnCode == "OK"):
            # Create canvas widget container for fig inside canvas frame and display it
            self.canvas = FigureCanvasTkAgg(self.fig,master=self.fCanvas)
            self.canvas.get_tk_widget().pack(side=tk.RIGHT)
            self.canvas.draw()
        else:
            mb.showerror("Error",self.returnCode)
            
        
    def savePNG(self):
        fileName=fd.asksaveasfilename(defaultextension=".png")
        self.fig.savefig(fileName, dpi=96)

    def savePDF(self):
        fileName=fd.asksaveasfilename(defaultextension=".pdf")
        self.fig.savefig(fileName)

    def _quit(self):
        app.quit()     # stops mainloop
        app.destroy()


if __name__ == "__main__":

    # Get arguments
    argv = sys.argv

    app = ExofingerGlovePattern()
    app.title("Exofinger Made to Measure Glove Pattern Generator")
    app.mainloop()
