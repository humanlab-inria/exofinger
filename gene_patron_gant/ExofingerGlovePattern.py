# This file is a part of Exofinger
#
# Copyright (C) 2021 Inria
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.

"""Exofinger made to measure glove pattern generator

Usage: python ExofingerGlovePattern.py
Author: L. Boissieux (2021)
"""
# Math functions
import math

# "Drawing" module
import matplotlib.pyplot as plt
import matplotlib as mpl

# Bezier curves lib module
from beziers.point import Point
from beziers.line import Line
from beziers.path import BezierPath
from beziers.cubicbezier import CubicBezier
from beziers.quadraticbezier import QuadraticBezier

# Target A3 paper size
# Figure is 41cm wide x 25cm high max
# height > 25cm -> change h/l ratio !!! even if aspect is set to 'equal', looks like monitor dependant
figW = 41 # cm
figH = 25 # cm max size with matplotlib for A3
cm2inch = 1/2.54 # inch per cm

colors = {'darkblue': '#384257', 'blue': '#567ac7','red': '#e63312', 'orange':'#f07e26', 'yellow':'#f07a26',
        'grey' : '#c7c6c4', 'white':'#fff' }

def rotatePoint(C, P, alpha):
    """
    rotatePoint
    in: C (Point) center of rotation
        P (Point) point to rotate
        alpha (float) angle of rotation (radians)
    out: rotated point  
    """
    dx = P.x - C.x
    dy = P.y - C.y
    return Point(C.x + dx * math.cos(alpha) - dy * math.sin(alpha), C.y + dx * math.sin(alpha) + dy * math.cos(alpha))

def getAngle(A,B,C,D):
    """
    getAngle
    in: A, B, C, D (Point) 
    out: return angle in radians between vectors AB and CD  
    """    
    return ( math.acos((((B.x - A.x) * (D.x - C.x)) + ((B.y - A.y) * (D.y - C.y)))/(Line(A,B).length * Line(C,D).length)) )

def solvePoly2(a, b, c):
    """
    solvePoly2
    in: a, b, c (float) 
    out: return solutions of ax2 + bx + c = 0 or 0,0 if no solution  
    """        
    delta = b**2 - (4 * a * c)
    try :        
        delta = math.sqrt(b**2 - (4 * a * c))
        x0 = ((-1.0 * b) + delta)/(2*a)
        x1 = ((-1.0 * b) - delta)/(2*a)       
        return x0, x1
    except ValueError :
        print ("no solution to ax2 + bx + c = 0")
        return 0, 0

def intersectCircles(C0, r0, C1, r1, leftside):
    """
    intersectCircles
    in: C0 (Point) center of 1st circle
        r0 (float) radius of 1sr circle
        C1 (Point) center of 2nd circle
        r1 (float) radius of 2ndcircle
        leftside (bool): true if the solution must be the leftest
    out: intersection (Point) of the 2 circles on the wanted side       
    """
    if C0.y != C1.y:
        dX = C0.x - C1.x
        dY = C0.y - C1.y
        fXY = dX / dY
        n = (r1**2 - r0**2 - C1.x**2 + C0.x**2 - C1.y**2 + C0.y**2) / (2 * dY)
        a = fXY**2 + 1
        b = (2 * C0.y * fXY) - (2 * n * fXY) - (2 * C0.x)
        c = C0.x**2 + C0.y**2 + n**2 - r0**2 - (2 * C0.y * n)
        x0, x1 = solvePoly2(a, b, c)
        I0 = Point(x0,n - (x0 * fXY))
        I1 = Point(x1,n - (x1 * fXY))        
    else:
        x = (r1**2 - r0**2 - C1.x**2 + C0.x**2)
        a = 1
        b = -1.0 * 2 * C1.y
        c = C1.x**2 + x**2 - (2 * C1.x * x) + C1.y**2 - r1**2
        y0,y1 = solvePoly2(a,b,c)
        I0 = Point(x,y0)
        I1 = Point(x,y1)
    if (leftside and I1.x > I0.x) or (not(leftside) and I1.x < I0.x) :
        return I0
    else:
        return I1


def intersectCircleLine(C, r, m, p, leftside):
    """
    intersectCircleLine
    in: C (Point) center of circle
        r (float) radius of circle
        m (float) directing coeff of line
        p (float) ordinate at origin of line
        leftside (bool): true if the solution must be the leftest
    out: P (Point) solution of the equation on chosen side        
    """
    a = 1 + m**2
    b = 2 * ((m * (p - C.y)) - C.x)
    c = C.x**2 + (p - C.y)**2 - r**2
    x0,x1 = solvePoly2(a,b,c)
    if (leftside and x1 > x0) or (not(leftside) and x1 < x0) :
        y = (m * x0) + p
        O = Point (x0,y)
    else:
        y = (m * x1) + p
        O = Point (x1,y)
    return O

def getLineCoeff(A, B):
    """
    getLineCoeff
    in: A, B (Point) 
    out: m, p (float) respectively slope and ordinate at origin of line AB
                      0 and A.y if A and B are vertically aligned
    """
    if (B.x - A.x != 0) :
        m = (B.y - A.y)/(B.x - A.x)
        p = B.y - (B.x * m)
    else : # vertical line
        m = 0
        p = A.y 
       
    return m, p
    
def orthoProjectOnLineFromCoeff(P, m, p):
    """
    orthoProjectOnLineFromCoeff
    in: P (Point) point to project
        m, p (float) respectively slope and ordinate at origin of line tp project on
    out: orthogonal projection of P on line (Point)     
    """    
    x = (P.x + (m * (P.y -p)))/(1 + m**2)
    y = m * x + p
    return Point(x, y)

def orthoProjectOnLine(P, A, B):
    """
    orthoProjectOnLine
    in: P (Point) point to project
        A, B (Point) points defining the line to project on \
    out: orthogonal projection of P on line AB (Point)     
    """    
    m, p = getLineCoeff (A, B)
    return orthoProjectOnLineFromCoeff(P, m, p)

    

def orthoProjectOnCircle(C, P, leftside):
    """
    orthoProjectOnCircle
    orthogonal projection of P on the circle with center C and radius CP gives
    2 solutions opposited on diameter
    the 3rd argument allows to choose one 
    in: C (Point) center of circle
        P (Point) point to project
        leftside (bool): true if the solution must be the leftest
    out: O (Point) solution of the equation on chosen side        
    """
    r = Line(C,P).length
    f = (P.y - C.y)/(P.x - C.x)
    a = r**2 / ( 1 + f**2 )
    y = math.sqrt(a) + C.y   
    x = C.x - (math.sqrt(a) * f)
    opx = (2 * C.x) - x # opposite on diameter
    opy = (2 * C.y) - y # opposite on diameter
    if (leftside and x > opx) or (not(leftside) and x < opx) :
        O = Point (opx,opy)
    else:       
        O = Point (x,y)
    return O

def getSymmetricalPoint(m,p,P) :
    """
    getSymmetricalPoint
    in: m (float) directing coefficient, 0 for vertical axis
        p (float) ordinate at origin, abscissa of certical axis
        P (Point) point to project
    out:  (Point) the symmetrical point of P relative the line y = mx + p
    """    
    if ( m != 0 ) :
        x = ( (2 * m * (P.y - p)) + (P.x * (1 - m**2)))/(1 + m**2)
        y = P.y + ((P.x - x)/m)
    else :
        x = (2 * p) - P.x
        y = P.y
    return Point(x, y)

def applySymmetryToAllPoints(A, B, points):
    """
    applySymmetryToAllPoints
    in: A, B (Point) points defining symmetry axis
        points dict of Points
    out: perform symmetry relative to line AB on each point of the dict   
    """
    m,p = getLineCoeff(A,B)
    for k in sorted(points.keys()):
        P = getSymmetricalPoint(m,p,points[k][0])
        points[k][0].x = P.x
        points[k][0].y = P.y
    

def drawSegments(ax, points):
    """
    drawSegments
    in: ax matplotlib axes object
        points dict of Point
    out: add closed plotting of the points to the matplot axes   
    """        
    lKeys = list(sorted(points.keys()))
    for i in range (0, len(lKeys) - 1):
        ax.plot([points[lKeys[i]][0].x,points[lKeys[i+1]][0].x],[points[lKeys[i]][0].y,points[lKeys[i+1]][0].y],color=points[lKeys[i]][1])
    # Close Pattern
    ax.plot([points[lKeys[i+1]][0].x,points[lKeys[0]][0].x],[points[lKeys[i+1]][0].y,points[lKeys[0]][0].y],color=points[lKeys[i+1]][1]) 

def drawVelcros(ax,P, width, length,orientation,layer,velcrosW):
    """
    drawVelcros
    in: ax (matplotlib axes object)
        P (Point) pivot point
        width (float) on which arrange several velcros strips
        length (float) of the velcro strip
        orientation (float) angle of the velcros strips
        layer (string) "U" for under then dasehed plotting, "A" for above then continuous plotting
        velcrosW (float) width of velcros strip, can be negative to arrange strips to the left of pivot point
        points dict of Point
    out: add closed plotting of the points to the matplot axes   
    """
    # Leave 0.5 cm around strips on 4 sides 
    nbV = int((width -1 )/ math.fabs(velcrosW)) 
    length = length -1
    # offset between strips
    delta = (width - ( nbV * math.fabs(velcrosW) + 1.0))/(nbV -1)
    # Position 1st strip 1st corner
    if velcrosW <0 :
        delta = -delta
        V = Point(P.x + 0.5, P.y + 0.5) 
    else :
        V = Point(P.x + 0.5, P.y - 0.5)
    points = []
    # Compute 1st strip points
    velcros = [ Point(V.x, V.y),Point(V.x + length, V.y), Point(V.x + length, V.y -velcrosW), Point(V.x, V.y -velcrosW) ]
    points.append(velcros)
    # Compute next strips points
    if nbV > 1:
        for i in range (0,nbV):
            V.y = V.y - velcrosW - delta
            vel = [ Point(V.x, V.y),Point(V.x + length, V.y), Point(V.x + length, V.y -velcrosW), Point(V.x, V.y -velcrosW) ]
            points.append(vel)
                      
    if layer == "U": # Velcros stitched under -> dashed plotting
        style = 'dashed'
    else: # velcros stitched above "A" -> continuous plotting
        style = 'solid'
    # Add labels, rotate strips and labels, plot to matplotlib object
    for v in range(0, nbV):
        anchor = Point(points[v][0].x + (length /2),points[v][0].y - (velcrosW/2))
        anchor = rotatePoint(P, anchor, orientation)
        ax.text(anchor.x, anchor.y,'Velcros ' + str(velcrosW) + 'cm x ' + str(math.fabs(round(length,1))) + 'cm' ,{'ha': 'center', 'va': 'center'},rotation=(180*orientation/math.pi))
        points[v][0] = rotatePoint(P,points[v][0], orientation)
        for i in range(1, 4):
            points[v][i] = rotatePoint(P, points[v][i], orientation)
            ax.plot([points[v][i-1].x,points[v][i].x],[points[v][i-1].y,points[v][i].y],linestyle=style,color='black')
        ax.plot([points[v][i].x,points[v][0].x],[points[v][i].y,points[v][0].y],linestyle=style,color='black')

def checkLimits(handValues, patternValues, version, drawHand, drawThumb, langId, alpha=math.pi/36):
    """
    checkLimits
    in: handValues dict of key/float
        patternValues dict of key/float
        version (int) version of pattern 
        alpha (float) angle used in pattern version 2
    out: returnCode (string)specifying if there are problems with current values that prevent from drawing the pattern, "OK" otherwise   
    """    

    global figW
    global figH
    returnCode = "OK"
    if 0.0 in handValues.values():
        if langId == 0:
            returnCode="Beware ! Found a nul hand measurement\nPlease check entries"
        else:
            returnCode="Attention ! Une des mesures est nulle\nVérifiez les entrées SVP"
        
    if(1.5 * handValues['Hand Contour'] + 3.5 > figW and version == 1) or (handValues['Hand Contour'] + handValues['Hand Back'] + 4 + math.sin(alpha)*handValues['Hand Side'] > figW and version == 2):
        if langId == 0:
            returnCode="Hand Contour over A3 printable size, max is 25cm (fig is max 41cm)"
        else:
            returnCode="Le tour de main est trop large pour imprimer en A3, le max est 25cm (le patron est limité à 41cm)"    
    
    if (handValues['Hand Length'] + handValues['Thumb Length'] + 2.0 > figH and drawHand and drawThumb and version == 1) or (1.5 * handValues['Hand Side'] + handValues['Thumb Length'] + 3.0 > figH and drawHand and drawThumb and version == 2) :
        if langId == 0:
            returnCode="Can't draw both patterns\nUncheck Thumb, draw, save\nUncheck Hand, check Thumb, draw, save"
        else:
            returnCode="Impossible de dessiner les 2 parties\nDésélectionnez le pouce, tracez, sauvez\nDésélectionnez la main, sélectionnez le pouce, tarcez, sauvez"

    if handValues['Wrist Contour'] <= handValues['Wrist Back']:
        if langId == 0:
            returnCode="Wrist Back must be shorter than Wrist Contour"
        else:
            returnCode="Le dos du poignet doit être plus court que le tour du poignet"
  
    if patternValues['Velcros Width'] > handValues['Wrist Back'] - 1.0:
        if langId == 0:
            returnCode="Velcros width is too high"
        else:
            returnCode="La largeur du Velcros est trop grande"
    return returnCode
    
def setFigure(drawGrid):
    """
    setFigure
    in: drawGrid (bool) add a grid to drawing if True
    out: matplot lib objects (plt, fig, ax) initialized according to global dimension values  
    """      
    global figW
    global figH
    global cm2inch
    
  # Specifying the width and the height of the box in inches
    fig = plt.figure(figsize=(figW * cm2inch, figH * cm2inch))
    ax = fig.add_axes((0, 0, 1, 1))
    ax.set_aspect('equal')
    ax.set(xlim=(0, figW),ylim=(0, figH))

    # Ticks settings
    ax.xaxis.set_major_locator(mpl.ticker.MultipleLocator(5))
    ax.xaxis.set_minor_locator(mpl.ticker.MultipleLocator(1))
    ax.yaxis.set_major_locator(mpl.ticker.MultipleLocator(5))
    ax.yaxis.set_minor_locator(mpl.ticker.MultipleLocator(1))

    if drawGrid:
        # Grid settings
        ax.grid(color="gray", which="both", linestyle=':', linewidth=0.5)
   
    return (plt, fig, ax)

def drawPattern(handValues, patternValues,hand,drawThumb,drawHand,drawGrid,langId,curveAccuracy=20):
    """
    Draw Exofinger pattern 
    in: handValues : dict associating measurement names and values in cm
        patternValues : dict associating patterns parameters names and values in cm
        hand (bool): which hand pattern to draw "Right" or "Left"
        drawThumb (bool): True to draw thumb pattern, False otherwise
        drawHand (bool): True to draw hand pattern, False otherwise
        drawGrid (bool): True to draw a 1x1cm grid, False otherwise
        langId (int): 0 for English, 1 for French
        curveAccuracy (int): resolution for Bezier curves sampling
    out: code (string): Error Code or "OK"
         plt : matplotlib plot object
         fig : matplotlib figure
        
    """

    global figW
    global figH
    global cm2inch
    returnCode = checkLimits(handValues, patternValues, 1, drawHand, drawThumb, langId)
    if returnCode == "OK" :
        plt, fig, ax = setFigure(drawGrid)
      
        try: 
            # Compute Points -------------------------------------------------------------------------------------------------------------
            handPoints = {}
            forefingerSide = handValues['Thumb Interior'] / 3
            if drawHand:
                # Hand
                handPoints['A'] = (Point(0.25, figH - 0.25),'black')
                handPoints['B'] = (Point(handPoints['A'][0].x + ((handValues['Hand Contour'] * 3) / 2) - 1.0, handPoints['A'][0].y), 'black')
                handPoints['C'] = (Point(handPoints['B'][0].x + 4.0, handPoints['A'][0].y), 'black')
                handPoints['D'] = (Point(handPoints['C'][0].x, handPoints['C'][0].y - 3.0), 'black')
                handPoints['E'] = (Point(handPoints['B'][0].x, handPoints['D'][0].y), 'black')        
                handPoints['F'] = (Point(handPoints['B'][0].x , handPoints['A'][0].y - handValues['Hand Length']), 'black')
                handPoints['K'] = (Point(handPoints['A'][0].x, handPoints['F'][0].y), 'black')
                handPoints['J'] = (Point(handPoints['K'][0].x + handValues['Wrist Back'], handPoints['K'][0].y), 'black')
                handPoints['I'] = (Point(handPoints['A'][0].x + handValues['Hand Back'], handPoints['A'][0].y - forefingerSide), colors['blue'])           
                handPoints['H'] = (Point(handPoints['J'][0].x + handValues['Thumb Base'], handPoints['J'][0].y - (0.5 * (handValues['Thumb Base'] / (handValues['Wrist Contour'] - handValues['Wrist Back'])))), colors['yellow'])           
                handPoints['G'] = (Point(handPoints['K'][0].x + handValues['Wrist Contour'], handPoints['K'][0].y - 0.5), 'black')   
                CIJ = Point(handPoints['J'][0].x,handPoints['I'][0].y) # control point for thumb clutch
                CHI = Point(handPoints['H'][0].x,handPoints['I'][0].y) # control point for thumb clutch
                handHI = QuadraticBezier(handPoints['H'][0],CHI,handPoints['I'][0])
                handIJ = QuadraticBezier(handPoints['I'][0],CIJ,handPoints['J'][0])
                for i in range(1, curveAccuracy):
                    handPoints['H' + str(i).zfill(2)] = (handHI.pointAtTime(i * (1/curveAccuracy)), colors['yellow'])
                for i in range(1, curveAccuracy):
                    handPoints['I' + str(i).zfill(2)] = (handIJ.pointAtTime(i * (1/curveAccuracy)), colors['blue']) 
            else :
                handPoints['G'] = (Point(0.3, figH - 0.3), 'black') 
                handPoints['I'] = (Point(handPoints['G'].x + (handValues['Hand Contour'] / 2), handPoints['G'].y - forefingerSide), colors['blue'])  
                handPoints['J'] = (Point(handPoints['G'].x + handValues['Wrist Back'], handPoints['G'].y - handValues['Hand Length']), 'black')              
                handPoints['H'] = (Point(handPoints['J'].x + handValues['Thumb Base'], handPoints['J'].y - (0.5 * (handValues['Thumb Base'] / (handValues['Wrist Contour'] - handValues['Wrist Back'])))), colors['yellow'])
                CIJ = Point(handPoints['J'].x,handPoints['I'].y) # control point for thumb clutch
                CHI = Point(handPoints['H'].x,handPoints['I'].y) # control point for thumb clutch
                
            thumbPoints = {} 
            if drawThumb:
                lHI = Line(handPoints['H'][0], handPoints['I'][0]).length
                lIJ = Line(handPoints['I'][0], handPoints['J'][0]).length
                # Thumb : compute vertically, in the middle of the figure width           
                thumbPoints['L'] = (Point(20.0 - (handValues['Thumb Contour']/2), handPoints['G'][0].y - 0.2), 'black') 
                thumbPoints['M'] = (Point(20.0 + (handValues['Thumb Contour']/2), handPoints['G'][0].y - 0.2), colors['red'])
                # Ensure computation of N and Q by reducing thumb length if necessary in a new variable but keeping original value for use in the other pattern version
                limit = handValues['Thumb Length'] - (handValues['Thumb Interior'] * 2/3)
                if limit > min(lHI,lIJ) :
                    thumbLength = handValues['Thumb Interior'] * 2/3 + min(lHI, lIJ) - 0.5
                    print("Need to decrease thumb length from ", handValues['Thumb Length'])
                    print("to ", thumbLength," to get a solution")
                    thumbPoints['O'] = (Point(20.0 + math.cos(math.pi / 30) * (handValues['Thumb Base'] * 3 /10), thumbPoints['L'][0].y - thumbLength - (math.sin(math.pi / 30) * handValues['Thumb Base'] * 3 /10) ), 'black') 
                    thumbPoints['P'] = (Point(20.0 - math.cos(math.pi / 30) * (handValues['Thumb Base'] * 7 /10), thumbPoints['L'][0].y - thumbLength + math.sin(math.pi / 30) * handValues['Thumb Base'] * 7 /10), colors['blue'])   
                else : 
                    thumbPoints['O'] = (Point(20.0 + math.cos(math.pi / 30) * (handValues['Thumb Base'] * 3 /10), thumbPoints['L'][0].y - handValues['Thumb Length'] - (math.sin(math.pi / 30) * handValues['Thumb Base'] * 3 /10) ), 'black') 
                    thumbPoints['P'] = (Point(20.0 - math.cos(math.pi / 30) * (handValues['Thumb Base'] * 7 /10), thumbPoints['L'][0].y - handValues['Thumb Length'] + math.sin(math.pi / 30) * handValues['Thumb Base'] * 7 /10), colors['blue'])   
                thumbPoints['N'] = (intersectCircles(thumbPoints['M'][0],handValues['Thumb Interior'] * 2/3,thumbPoints['O'][0],lHI, False), colors['yellow'])
                center = Point(thumbPoints['N'][0].x + ((thumbPoints['M'][0].x - thumbPoints['N'][0].x) / 5), thumbPoints['N'][0].y +  ((thumbPoints['M'][0].y - thumbPoints['N'][0].y) /5))
                CMN = orthoProjectOnCircle(center, thumbPoints['N'][0], True)
                thumbMN = QuadraticBezier(thumbPoints['M'][0],CMN,thumbPoints['N'][0])
                for i in range(1, curveAccuracy):
                    thumbPoints['M' + str(i).zfill(2)] = (thumbMN.pointAtTime(i * (1/curveAccuracy)), colors['red'])
                lICHI = Line(handPoints['I'][0],CHI).length
                lCHIH = Line(CHI,handPoints['H'][0]).length
                CNO = intersectCircles(thumbPoints['N'][0],lICHI,thumbPoints['O'][0],lCHIH, False)
                thumbNO = QuadraticBezier(thumbPoints['N'][0],CNO,thumbPoints['O'][0])   
                for i in range(1, curveAccuracy):
                    thumbPoints['N'+ str(i).zfill(2)] = (thumbNO.pointAtTime(i * (1/curveAccuracy)),  colors['yellow'])
                thumbPoints['Q'] = (intersectCircles(thumbPoints['L'][0],handValues['Thumb Interior'] * 2/3,thumbPoints['P'][0],lIJ, True), colors['red'])
                
                lICIJ = Line(handPoints['I'][0],CIJ).length
                lCIJJ = Line(CIJ,handPoints['J'][0]).length
                CPQ= intersectCircles(thumbPoints['Q'][0],lICIJ,thumbPoints['P'][0],lCIJJ, True)

                thumbPQ = QuadraticBezier(thumbPoints['P'][0],CPQ,thumbPoints['Q'][0])
                
                for i in range(1, curveAccuracy):
                    thumbPoints['P'+ str(i).zfill(2)] = (thumbPQ.pointAtTime(i * (1/curveAccuracy)), colors['blue'])
             
                center = Point(thumbPoints['Q'][0].x + ((thumbPoints['L'][0].x - thumbPoints['Q'][0].x) / 5), thumbPoints['Q'][0].y +  ((thumbPoints['L'][0].y - thumbPoints['Q'][0].y) /5))
                CQL = orthoProjectOnCircle(center, thumbPoints['Q'][0], False)
                thumbQL = QuadraticBezier(thumbPoints['Q'][0],CQL,thumbPoints['L'][0])
                for i in range(1, curveAccuracy):
                    thumbPoints['Q'+ str(i).zfill(2)] = (thumbQL.pointAtTime(i * (1/curveAccuracy)), colors['red'])
                TSLength = thumbQL.length

                # Translate thumb pattern
                for k in sorted(thumbPoints.keys()):
                    thumbPoints[k][0].x = thumbPoints[k][0].x - CPQ.x + 0.25             
            else :
                thumbPoints['P'] = (Point(-0.5, figH - handValues['Hand Length'] -3.5), colors['blue'])
                thumbPoints['Q'] = (Point(thumbPoints['P'].x,thumbPoints['P'].y), colors['orange'])
                thumbPoints['N'] = (Point(thumbPoints['P'].x,thumbPoints['P'].y),  colors['yellow'])            

           
                
        except ValueError:
            if langId == 0:
                returnCode="Can't compute pattern due to incoherent measurements"
            else:
                returnCode="Des mesures incohérentes empêchent de calculer le patron"
            return (returnCode,None, None)

        symXFactor = 1
        if (hand == "Left"):
            symXFactor = -1
            if drawHand:
                center = handPoints['A'][0] + (( handPoints['C'][0] - handPoints['A'][0] ) * 0.5)
                applySymmetryToAllPoints(center, center + Point(0,1), handPoints)
                # Translate Hand pattern points to the left
                xoffset = handPoints['D'][0].x - 0.25     
                for k in sorted(handPoints.keys()):
                    handPoints[k][0].x = handPoints[k][0].x - xoffset
            if drawThumb:
                center = thumbPoints['Q'][0]+ (( thumbPoints['N'][0] - thumbPoints['Q'][0] ) * 0.5)                              
                applySymmetryToAllPoints(center, center + Point(0,1), thumbPoints)
                # Translate Thumb pattern points to the left
                xoffset = thumbPoints['N'][0].x - 1.5     
                for k in sorted(thumbPoints.keys()):
                    thumbPoints[k][0].x = thumbPoints[k][0].x - xoffset

        # Sheathes Points
        if hand == "Left":
            HS = Point(thumbPoints['Q'][0].x + 1, thumbPoints['P'][0].y)
        else:
            HS = Point(thumbPoints['N'][0].x + 1, thumbPoints['P'][0].y)
            
        # Drawing
        if drawHand:
            drawSegments(ax, handPoints)
            # Velcros
            if symXFactor == -1:
                drawVelcros(ax,handPoints['K'][0], handValues['Hand Length'], handValues['Wrist Back'], math.pi, "U", patternValues['Velcros Width'])
            else :
                drawVelcros(ax,handPoints['A'][0], handValues['Hand Length'], handValues['Wrist Back'], 0 , "U", patternValues['Velcros Width'])
            drawVelcros(ax,handPoints['F'][0], handValues['Wrist Back'], handValues['Hand Length'], math.pi/2, "A", - symXFactor * patternValues['Velcros Width'])

            # Sheath Path 
            CIS = Point(handPoints['G'][0].x + ((handPoints['F'][0].x - handPoints['G'][0].x)/2), handPoints['I'][0].y) # control point
            S = Point(CIS.x, handPoints['G'][0].y + ((handPoints['F'][0].y - handPoints['F'][0].y)/2))
            sheathPath = QuadraticBezier(handPoints['I'][0],CIS,S)
            ISPoints = []
            for i in range(0, curveAccuracy):
                ISPoints.append(sheathPath.pointAtTime(i * (1/curveAccuracy)))
            ISPoints.append(S)
            for i in range (0, len(ISPoints) - 1):
                ax.plot([ISPoints[i].x,ISPoints[i+1].x],[ISPoints[i].y,ISPoints[i+1].y],color=colors['grey'],linestyle='dashed') 
            if langId == 0:
                label = "Sheath Path"
            else :
                label = "Chemin de gaine"    
            ax.text(handPoints['I'][0].x + symXFactor * 3 , handPoints['I'][0].y + patternValues['Text Offset'], label,color=colors['grey'])
            
            # Hand Sheath        
            ax.plot([HS.x,HS.x + sheathPath.length,HS.x + sheathPath.length,HS.x,HS.x],[HS.y,HS.y,HS.y + 2.0,HS.y + 2.0, HS.y],color='black')
            if langId == 0:
                label = "Hand Sheath"
            else :
                label = "Gaine de la main"
            ax.text(HS.x + (sheathPath.length /2),HS.y + 1,label,{'ha': 'center', 'va': 'center'})
            
            # Annotations
            ax.text(handPoints['A'][0].x + symXFactor * 2 *patternValues['Text Offset'], handPoints['A'][0].y - 2* patternValues['Text Offset'], 'A')
            ax.text(handPoints['B'][0].x , handPoints['B'][0].y - 2* patternValues['Text Offset'], 'B',{'ha': 'center', 'va': 'top'})
            ax.text(handPoints['C'][0].x - symXFactor * 2 *patternValues['Text Offset'], handPoints['C'][0].y - 2 * patternValues['Text Offset'], 'C')
            ax.text(handPoints['D'][0].x - symXFactor * 2 *patternValues['Text Offset'], handPoints['D'][0].y + patternValues['Text Offset'], 'D')
            ax.text(handPoints['E'][0].x , handPoints['E'][0].y + patternValues['Text Offset'], 'E',{'ha': 'center', 'va': 'bottom'})
            ax.text(handPoints['F'][0].x - symXFactor * 2 * patternValues['Text Offset'], handPoints['F'][0].y + patternValues['Text Offset'], 'F',{'ha': 'right', 'va': 'bottom'})
            ax.text(handPoints['G'][0].x , handPoints['G'][0].y + patternValues['Text Offset'], 'G',{'ha': 'center', 'va': 'bottom'})
            ax.text(handPoints['H'][0].x + symXFactor * 2 * patternValues['Text Offset'], handPoints['H'][0].y + patternValues['Text Offset'], 'H')
            ax.text(handPoints['I'][0].x, handPoints['I'][0].y + patternValues['Text Offset'], 'I',{'ha': 'center', 'va': 'bottom'})
            ax.text(handPoints['J'][0].x - symXFactor * patternValues['Text Offset'], handPoints['J'][0].y + patternValues['Text Offset'], 'J')
            ax.text(handPoints['K'][0].x + symXFactor * 2 * patternValues['Text Offset'], handPoints['K'][0].y + patternValues['Text Offset'], 'K')    
            # Ticks
            ax.plot([handPoints['B'][0].x,handPoints['B'][0].x],[handPoints['B'][0].y,handPoints['B'][0].y - patternValues['Text Offset'] +0.1],color='black')
            ax.plot([handPoints['I'][0].x,handPoints['I'][0].x],[handPoints['I'][0].y,handPoints['I'][0].y + patternValues['Text Offset'] -0.1],color='black')
            ax.plot([handPoints['G'][0].x,handPoints['G'][0].x],[handPoints['G'][0].y,handPoints['G'][0].y + patternValues['Text Offset'] -0.1],color='black')
            # Pattern name
            if langId == 0:
                name = hand + ' Hand Reverse'
            else:
                if hand == "Left":
                    name = "Envers main gauche"
                else :
                    name = "Envers main droite"
            ax.text(handPoints['G'][0].x, handPoints['G'][0].y + (handValues['Hand Length']/2), name,{'ha': 'center', 'va': 'center'})

        if drawThumb:
            drawSegments(ax, thumbPoints)
            # Annotations
            ax.text(thumbPoints['L'][0].x + symXFactor * patternValues['Text Offset'], thumbPoints['L'][0].y - 2* patternValues['Text Offset'], 'L')
            ax.text(thumbPoints['M'][0].x - symXFactor * 2 * patternValues['Text Offset'], thumbPoints['M'][0].y - 2* patternValues['Text Offset'], 'M')
            ax.text(thumbPoints['N'][0].x - symXFactor * patternValues['Text Offset'] , thumbPoints['N'][0].y - patternValues['Text Offset'], 'N',{'ha': 'right', 'va': 'top'})
            ax.text(thumbPoints['O'][0].x , thumbPoints['O'][0].y + patternValues['Text Offset'], 'O',{'ha': 'center', 'va': 'bottom'})
            ax.text(thumbPoints['P'][0].x , thumbPoints['P'][0].y +  patternValues['Text Offset'], 'P',{'ha': 'center', 'va': 'bottom'})
            ax.text(thumbPoints['Q'][0].x + symXFactor * 2 * patternValues['Text Offset'], thumbPoints['Q'][0].y - patternValues['Text Offset'] , 'Q',{'ha': 'left', 'va': 'top'})
            # Ticks
            ax.plot([thumbPoints['Q'][0].x,thumbPoints['Q'][0].x + symXFactor * patternValues['Text Offset']],[thumbPoints['Q'][0].y,thumbPoints['Q'][0].y - patternValues['Text Offset']],color='black')
            ax.plot([thumbPoints['N'][0].x,thumbPoints['N'][0].x - symXFactor * patternValues['Text Offset']],[thumbPoints['N'][0].y,thumbPoints['N'][0].y - patternValues['Text Offset']],color='black')
            
            # Pattern name
            if langId == 0:
                name = hand + ' Thumb Reverse'
            else:
                if hand == "Left":
                    name = "Envers pouce gauche"
                else :
                    name = "Envers pouce droit"
            ax.text(thumbPoints['P'][0].x + 0.5 * (thumbPoints['O'][0].x - thumbPoints['P'][0].x), thumbPoints['P'][0].y + (handValues['Thumb Length']/2),name,{'ha': 'center', 'va': 'center'})
            if hand == "Left":
                anchor = Point(thumbPoints['Q'][0].x + 1, thumbPoints['M'][0].y -1)
            else:
                anchor = Point(thumbPoints['N'][0].x + 1, thumbPoints['M'][0].y -1)
            if langId == 0 :
                text = "-Trace the pattern on the fabric reverse\n-Add a seam allowance as it isn\'t included\n-Sew DC on EB\n        QL on NM\n        PQ on JI\n        NO on IH\n"
            else :
                text = "-Tracer le patron sur l'envers du tissu\n-Ajouter les marges de coutures (non incluses dans le patron)\n-Coudre DC sur EB\n             QL sur NM\n             PQ sur JI\n             NO sur IH\n"

            ax.text(anchor.x,anchor.y, text,{'ha': 'left', 'va': 'top'})  
            # Thumb sheath
            if not drawHand:
                sheathPath = Line(Point(0,0),Point(8,0))
            TS = Point(HS.x + sheathPath.length, HS.y + 2.5)
            ax.plot([TS.x,TS.x,TS.x - TSLength,TS.x - TSLength,TS.x],[TS.y,TS.y + 2.0,TS.y + 2.0,TS.y, TS.y],color='black')
            if langId == 0:
                label = "Thumb Sheath"
            else :
                label = "Gaine du pouce"
            ax.text(TS.x - (TSLength /2),TS.y + 1,label,{'ha': 'center', 'va': 'center'})
              
        return (returnCode,plt, fig)
    else:
        if langId == 0:
            returnCode = "Le patron excède le format A3, générer main et pouce indépendamment"
        else :
            returnCode = "Patterns exceed A3, draw hand and thumb independantly"
        return (returnCode, None,None)

    
def drawPatternV2(handValues, patternValues,hand,drawThumb,drawHand,drawGrid,langId,curveAccuracy=20):
    """
    Draw Exofinger pattern 
    in: handValues : dict associating measurement names and values in cm
        patternValues : dict associating patterns parameters names and values in cm
        hand (bool): which hand pattern to draw "Right" or "Left"
        drawThumb (bool): True to draw thumb pattern, False otherwise
        drawHand (bool): True to draw hand pattern, False otherwise
        drawGrid (bool): True to draw a 1x1cm grid, False otherwise
        langId (int): 0 for English, 1 for French
        curveAccuracy (int): resolution for Bezier curves sampling
    out: code (string): Error Code or "OK"
         plt : matplotlib plot object
         fig : matplotlib figure
        
    """
    global figW
    global figH
    global cm2inch
    alpha = math.pi/36 # angle used for hand pattern
    returnCode = checkLimits(handValues, patternValues, 2, drawHand, drawThumb, langId, alpha)
    if returnCode == "OK" :
        plt, fig, ax = setFigure(drawGrid)

        try: 
            # Compute Points -------------------------------------------------------------------------------------------------------------
            thumbPoints = {}         
            handPoints = {}
            forefingerSide = handValues['Thumb Interior'] / 3.0

            # Thumb : compute vertically, in the middle of the figure width
            Orig=Point(figW/2,(figH/2) + handValues['Thumb Proximal'])
            LMPO=Point(Orig.x, Orig.y - handValues['Thumb Proximal'])
            CPO = Point(Orig.x, Orig.y - handValues['Thumb Length'])
            thumbPoints['L'] = (Point(Orig.x - ((handValues['Thumb Contour']/2) * math.cos(math.pi / 18)), Orig.y + ((handValues['Thumb Contour']/2) * math.sin(math.pi / 18))), 'black') 
            thumbPoints['M'] = (Point(Orig.x + ((handValues['Thumb Contour']/2) * math.cos(math.pi / 18)), thumbPoints['L'][0].y), colors['red'])      
            thumbPoints['O'] = (Point(CPO.x + ((handValues['Thumb Base'] * 3 / 5) * math.cos(math.pi / 9)), CPO.y - ((handValues['Thumb Base'] * 3 / 5) * math.sin(math.pi / 9))), 'black')
            thumbPoints['P'] = (Point(CPO.x - ((handValues['Thumb Base'] * 2 / 5) * math.cos(math.pi / 18)), CPO.y + ((handValues['Thumb Base'] * 2 / 5) * math.sin(math.pi / 18))), colors['blue'])
            CLM = Point(Orig.x, 2* Orig.y - thumbPoints['L'][0].y)
            thumbLM = QuadraticBezier(thumbPoints['L'][0],CLM,thumbPoints['M'][0])
            for i in range(1, curveAccuracy):
                thumbPoints['L' + str(i).zfill(2)] = (thumbLM.pointAtTime(i * (1/curveAccuracy)),'black')
            CPO.y = thumbPoints['P'][0].y
            thumbOP = QuadraticBezier(thumbPoints['O'][0],CPO,thumbPoints['P'][0])
            for i in range(1, curveAccuracy):
                thumbPoints['O' + str(i).zfill(2)] = (thumbOP.pointAtTime(i * (1/curveAccuracy)),'black')

            m, p = getLineCoeff(Orig, thumbPoints['M'][0])
            p = LMPO.y - (LMPO.x * m)
            CMN = intersectCircleLine(thumbPoints['M'][0],handValues['Thumb Proximal'], m, p, False)
            thumbPoints['N'] = (Point(CMN.x + (2 * forefingerSide - handValues['Thumb Proximal']), CMN.y),colors['yellow'])
            thumbMN = QuadraticBezier(thumbPoints['M'][0],CMN,thumbPoints['N'][0])
            # Ensure length of curve is 2/3 Thumb Interior
            while thumbMN.length < 2 * forefingerSide :
                thumbPoints['N'][0].x += 0.1
                thumbMN = QuadraticBezier(thumbPoints['M'][0],CMN,thumbPoints['N'][0])
            for i in range(1, curveAccuracy):
                thumbPoints['M' + str(i).zfill(2)] = (thumbMN.pointAtTime(i * (1/curveAccuracy)), colors['red']) 
            CNOX = Point(thumbPoints['O'][0].x + (thumbPoints['N'][0].x - thumbPoints['O'][0].x)/3, thumbPoints['O'][0].y)
            CNOY = Point(thumbPoints['N'][0].x, thumbPoints['N'][0].y - (thumbPoints['N'][0].y - thumbPoints['O'][0].y)/3) 
            thumbNO = CubicBezier(thumbPoints['N'][0],CNOY,CNOX,thumbPoints['O'][0])   
            for i in range(1, curveAccuracy):
                thumbPoints['N'+ str(i).zfill(2)] = (thumbNO.pointAtTime(i * (1/curveAccuracy)),  colors['yellow'])
        
            thumbPoints['Q'] = (getSymmetricalPoint(0, Orig.x, thumbPoints['N'][0]), colors['red'])
            
            CPQX = Point(thumbPoints['Q'][0].x + (thumbPoints['P'][0].x - thumbPoints['Q'][0].x)/3, thumbPoints['Q'][0].y)
            CPQY = Point(thumbPoints['P'][0].x, thumbPoints['P'][0].y + (thumbPoints['Q'][0].y - thumbPoints['P'][0].y)/3)
            thumbPQ = CubicBezier(thumbPoints['P'][0],CPQY,CPQX,thumbPoints['Q'][0])          
            for i in range(1, curveAccuracy):
                thumbPoints['P'+ str(i).zfill(2)] = (thumbPQ.pointAtTime(i * (1/curveAccuracy)), colors['blue'])
         
            CQL = getSymmetricalPoint(0, Orig.x, CMN)
            thumbQL = QuadraticBezier(thumbPoints['Q'][0],CQL,thumbPoints['L'][0])
            for i in range(1, curveAccuracy):
                thumbPoints['Q'+ str(i).zfill(2)] = (thumbQL.pointAtTime(i * (1/curveAccuracy)), colors['red'])
            TSLength = thumbQL.length
           
            # Hand
            handPoints['A01'] = (Point(figW / 2, figH / 2), 'black')
            handPoints['A02'] = (Point(handPoints['A01'][0].x + handValues['Hand Contour'] - handValues['Hand Back'], handPoints['A01'][0].y), 'black')           
            handPoints['B'] = (Point(handPoints['A02'][0].x + (handValues['Hand Back'] - 1) * math.cos(2*alpha), handPoints['A02'][0].y + (handValues['Hand Back'] - 1) * math.sin(2*alpha)), 'black')
            handPoints['C'] = (Point(handPoints['B'][0].x + 4 * math.cos(2*alpha), handPoints['B'][0].y + 4 * math.sin(2*alpha)), 'black')
            handPoints['E'] = (Point(handPoints['B'][0].x + 3 * math.sin(2*alpha), handPoints['B'][0].y - 3 * math.cos(2*alpha)), 'black')
            handPoints['D'] = (Point(handPoints['E'][0].x + 4 * math.cos(3*alpha), handPoints['E'][0].y + 4 * math.sin(3*alpha)), 'black')

            handPoints['G'] = (Point(handPoints['A02'][0].x + handValues['Hand Side'] * math.sin(2*alpha), handPoints['A02'][0].y - handValues['Hand Side'] * math.cos(2*alpha)), 'black')
            handPoints['F'] = (Point(handPoints['G'][0].x + handValues['Wrist Back'] * math.cos(2*alpha), handPoints['G'][0].y + handValues['Wrist Back'] * math.sin(2*alpha)), 'black')
                                 
            handPoints['A'] = (Point(handPoints['A01'][0].x - handValues['Hand Back'] * math.cos(2*alpha), handPoints['A01'][0].y + handValues['Hand Back'] * math.sin(2*alpha)), 'black')
            handPoints['I'] = (Point(handPoints['A01'][0].x - forefingerSide * math.sin(alpha), handPoints['A01'][0].y - forefingerSide * math.cos(alpha)), colors['blue'])                  
            handPoints['K'] = (Point(handPoints['A'][0].x - handValues['Hand Side'] * math.sin(4*alpha), handPoints['A'][0].y - handValues['Hand Side'] * math.cos(2*alpha)), 'black')
                                 
            lPQ = Line(thumbPoints['P'][0],thumbPoints['Q'][0]).length
            handPoints['J'] = (intersectCircles(handPoints['I'][0], lPQ, handPoints['K'][0], handValues['Wrist Back'], False), 'black')         
            angle = getAngle(thumbPoints['P'][0], thumbPoints['Q'][0], handPoints['J'][0], handPoints['I'][0])
            translation = handPoints['J'][0] - thumbPoints['P'][0]
            m, p = getLineCoeff(handPoints['J'][0], handPoints['I'][0])
            CIJX = CPQX + translation
            CIJX = rotatePoint(handPoints['J'][0], CIJX, -angle)
            CIJX = getSymmetricalPoint(m,p,CIJX)
            #ax.scatter(CIJX.x,CIJX.y, s=30, color='green')
            CIJY = CPQY + translation
            CIJY = rotatePoint(handPoints['J'][0], CIJY, -angle)
            CIJY = getSymmetricalPoint(m,p,CIJY)
            #ax.scatter(CIJY.x,CIJY.y, s=10, color='green')

            lNO = Line(thumbPoints['N'][0],thumbPoints['O'][0]).length
            lIG = Line(handPoints['I'][0],handPoints['G'][0]).length
            # Ensure computation of H by decreasing thumb base length if necessary
            lGH = handValues['Wrist Contour'] - handValues['Wrist Back'] - handValues['Thumb Base']
            if lGH + lNO < lIG + 0.5 : # lGH + lNO must sufficiently < lIG to get intersection and I, H, G not aligned
                print("Need to raise (Wrist Contour - Wrist Back - Thumb Base from ", lGH)
                lGH = lIG + 0.5 -  lNO
                print("to ", lGH," to get a solution")
            handPoints['H'] = (intersectCircles(handPoints['I'][0], lNO, handPoints['G'][0],lGH , True), colors['yellow']) 
            angle = getAngle(thumbPoints['O'][0], thumbPoints['N'][0], handPoints['H'][0], handPoints['I'][0])
            translation = handPoints['H'][0] - thumbPoints['O'][0]
            CHIX = CNOY + translation
            CHIX = rotatePoint(handPoints['H'][0], CHIX, angle)
            CHIY = CNOX + translation
            CHIY = rotatePoint(handPoints['H'][0], CHIY, angle)             
            handHI = CubicBezier(handPoints['H'][0],CHIY,CHIX,handPoints['I'][0])
            handIJ = CubicBezier(handPoints['I'][0],CIJX,CIJY,handPoints['J'][0])
            for i in range(1, curveAccuracy):
                handPoints['H' + str(i).zfill(2)] = (handHI.pointAtTime(i * (1/curveAccuracy)), colors['yellow'])
            for i in range(1, curveAccuracy):
                handPoints['I' + str(i).zfill(2)] = (handIJ.pointAtTime(i * (1/curveAccuracy)), colors['blue'])

            # Adjust Hand pattern over x axis     
            xoffset = handPoints['K'][0].x - 0.25
            for k in sorted(handPoints.keys()):
                handPoints[k][0].x = handPoints[k][0].x - xoffset
                 
        except ValueError as e:
            if langId == 0:
                returnCode="Can't compute pattern due to incoherent measurements"
            else:
                returnCode="Des mesures incohérentes empêchent de calculer le patron"
            print (e)
            return (returnCode,None, None)

        symXFactor = 1
        if (hand == "Left"):
            symXFactor = -1
            if drawHand:
                center = handPoints['A'][0] + (( handPoints['C'][0] - handPoints['A'][0] ) * 0.5)
                applySymmetryToAllPoints(center, center + Point(0,1), handPoints)
                # Translate Hand pattern points to the left
                xoffset = handPoints['D'][0].x - 0.5     
                for k in sorted(handPoints.keys()):
                    handPoints[k][0].x = handPoints[k][0].x - xoffset
            if drawThumb:
                center =  thumbPoints['P'][0] + (( thumbPoints['M'][0] - thumbPoints['P'][0] ) * 0.5)
                applySymmetryToAllPoints(center, center + Point(0,1), thumbPoints)

        # Translate thumb pattern
        xoffset = thumbPoints['O'][0].x - handPoints['A02'][0].x 
        yoffset = figH - thumbPoints['L'][0].y - 0.25
        for k in sorted(thumbPoints.keys()):
            thumbPoints[k][0].x = thumbPoints[k][0].x - xoffset
            thumbPoints[k][0].y = thumbPoints[k][0].y + yoffset

        if drawThumb :   
        # Translate hand pattern under thumb pattern       
            yoffset = thumbPoints['O'][0].y - handPoints['A02'][0].y - 0.25           
        else :
            yoffset = figH - handPoints['C'][0].y - 0.25
        for k in sorted(handPoints.keys()):
            handPoints[k][0].y = handPoints[k][0].y + yoffset   

        # Sheathes Points
        if hand == "Left":
            TS = Point(thumbPoints['Q'][0].x + 0.5, thumbPoints['M'][0].y)
        else:
            TS = Point(thumbPoints['N'][0].x + 0.5, thumbPoints['M'][0].y)
        HSY = min(handPoints['J'][0].y, handPoints['K'][0].y)
        HS = Point(handPoints['K'][0].x, HSY - 1)    
        # Drawing
        if drawHand:
            drawSegments(ax, handPoints)
            # Velcros
            angle = getAngle(handPoints['A'][0], handPoints['K'][0], handPoints['A'][0], handPoints['A'][0] + Point(0,-1))
            if symXFactor == -1 :
                drawVelcros(ax,handPoints['A'][0], handValues['Hand Side'], handValues['Wrist Back'], math.pi + angle, "U", symXFactor * patternValues['Velcros Width'])
            else :
                drawVelcros(ax,handPoints['A'][0], handValues['Hand Side'], handValues['Wrist Back'], -angle , "U", symXFactor * patternValues['Velcros Width'])
            PF = orthoProjectOnLine(handPoints['F'][0], handPoints['A02'][0], handPoints['B'][0])
            drawVelcros(ax,handPoints['F'][0], handValues['Wrist Back'], math.sqrt((handPoints['F'][0].x - PF.x)**2 + (handPoints['F'][0].y - PF.y)**2) , (math.pi /2) + symXFactor * (2 * alpha), "A", - symXFactor * patternValues['Velcros Width'])
            
            # Sheath Path            
            S = Point((handPoints['G'][0].x + handPoints['F'][0].x) / 2, (handPoints['G'][0].y + handPoints['F'][0].y) /2)
            CIS = orthoProjectOnLine(S, handPoints['A02'][0], handPoints['B'][0])
            #ax.scatter(CIS.x,CIS.y, s=20)
            sheathPath = QuadraticBezier(S,CIS,handPoints['I'][0])
            ISPoints = []
            for i in range(0, curveAccuracy):
                ISPoints.append(sheathPath.pointAtTime(i * (1/curveAccuracy)))
            ISPoints.append(handPoints['I'][0])
            for i in range (0, len(ISPoints) - 1):
                ax.plot([ISPoints[i].x,ISPoints[i+1].x],[ISPoints[i].y,ISPoints[i+1].y],color=colors['grey'],linestyle='dashed')
            if langId == 0:
                label = "Sheath Path"
            else :
                label = "Chemin de gaine"    
            ax.text(handPoints['I'][0].x + symXFactor * 3 , handPoints['I'][0].y + patternValues['Text Offset'], label,color=colors['grey'])

            # Hand Sheath        
            ax.plot([HS.x,HS.x + symXFactor * sheathPath.length,HS.x + symXFactor * sheathPath.length,HS.x,HS.x],[HS.y,HS.y,HS.y - 2.0,HS.y - 2.0, HS.y],color='black')
            if langId == 0:
                label = "Hand Sheath"
            else :
                label = "Gaine de la main"
            ax.text(HS.x + symXFactor * (sheathPath.length /2),HS.y - 1,label,{'ha': 'center', 'va': 'center'})
            
            # Annotations
            ax.text(handPoints['A'][0].x, handPoints['A'][0].y - 2* patternValues['Text Offset'], 'A', {'ha': 'center', 'va': 'top'})
            ax.text(handPoints['B'][0].x, handPoints['B'][0].y - 2* patternValues['Text Offset'], 'B',{'ha': 'center', 'va': 'top'})
            ax.text(handPoints['C'][0].x - symXFactor * 2 *patternValues['Text Offset'], handPoints['C'][0].y - 2 * patternValues['Text Offset'], 'C',{'ha': 'center', 'va': 'top'})
            ax.text(handPoints['D'][0].x - symXFactor * 2 *patternValues['Text Offset'], handPoints['D'][0].y, 'D',{'ha': 'center', 'va': 'bottom'})
            ax.text(handPoints['E'][0].x, handPoints['E'][0].y + patternValues['Text Offset'], 'E',{'ha': 'center', 'va': 'bottom'})
            ax.text(handPoints['F'][0].x - symXFactor * 2 * patternValues['Text Offset'], handPoints['F'][0].y + patternValues['Text Offset'], 'F',{'ha': 'center', 'va': 'bottom'})
            ax.text(handPoints['G'][0].x, handPoints['G'][0].y + patternValues['Text Offset'], 'G',{'ha': 'center', 'va': 'bottom'})
            ax.text(handPoints['H'][0].x + symXFactor * 2 * patternValues['Text Offset'], handPoints['H'][0].y + 2 * patternValues['Text Offset'], 'H',{'ha': 'center', 'va': 'bottom'})
            ax.text(handPoints['I'][0].x, handPoints['I'][0].y + patternValues['Text Offset'], 'I',{'ha': 'center', 'va': 'bottom'})
            ax.text(handPoints['J'][0].x, handPoints['J'][0].y + 2 * patternValues['Text Offset'], 'J',{'ha': 'center', 'va': 'bottom'})
            ax.text(handPoints['K'][0].x + symXFactor * 2 * patternValues['Text Offset'], handPoints['K'][0].y, 'K',{'ha': 'center', 'va': 'center'})    
            # Ticks
            ax.plot([handPoints['B'][0].x,handPoints['B'][0].x],[handPoints['B'][0].y,handPoints['B'][0].y - patternValues['Text Offset'] +0.1],color='black')
            ax.plot([handPoints['I'][0].x,handPoints['I'][0].x],[handPoints['I'][0].y,handPoints['I'][0].y + patternValues['Text Offset'] -0.1],color='black')
            ax.plot([handPoints['G'][0].x,handPoints['G'][0].x],[handPoints['G'][0].y,handPoints['G'][0].y + patternValues['Text Offset'] -0.1],color='black')
            # Pattern name
            if langId == 0:
                name = hand + ' Hand Reverse'
            else:
                if hand == "Left":
                    name = "Envers main gauche"
                else :
                    name = "Envers main droite"
            ax.text(handPoints['H'][0].x, handPoints['H'][0].y + (handValues['Hand Side']/2), name,{'ha': 'center', 'va': 'center'})

        if drawThumb:
            drawSegments(ax, thumbPoints)
            # Annotations
            ax.text(thumbPoints['L'][0].x + symXFactor * patternValues['Text Offset'], thumbPoints['L'][0].y - 2* patternValues['Text Offset'], 'L',{'ha': 'center', 'va': 'top'})
            ax.text(thumbPoints['M'][0].x - symXFactor * 2 * patternValues['Text Offset'], thumbPoints['M'][0].y - 2 * patternValues['Text Offset'], 'M',{'ha': 'center', 'va': 'top'})
            ax.text(thumbPoints['N'][0].x - symXFactor * 2 * patternValues['Text Offset'] , thumbPoints['N'][0].y - patternValues['Text Offset'], 'N',{'ha': 'center', 'va': 'top'})
            ax.text(thumbPoints['O'][0].x , thumbPoints['O'][0].y + patternValues['Text Offset'], 'O',{'ha': 'center', 'va': 'bottom'})
            ax.text(thumbPoints['P'][0].x + symXFactor * 2 * patternValues['Text Offset'], thumbPoints['P'][0].y +  patternValues['Text Offset'], 'P',{'ha': 'center', 'va': 'bottom'})
            ax.text(thumbPoints['Q'][0].x + symXFactor * 5 * patternValues['Text Offset'], thumbPoints['Q'][0].y, 'Q',{'ha': 'center', 'va': 'center'})
            # Ticks
            ax.plot([thumbPoints['Q'][0].x,thumbPoints['Q'][0].x + symXFactor * patternValues['Text Offset']],[thumbPoints['Q'][0].y,thumbPoints['Q'][0].y - patternValues['Text Offset']],color='black')
            ax.plot([thumbPoints['N'][0].x,thumbPoints['N'][0].x - symXFactor * patternValues['Text Offset']],[thumbPoints['N'][0].y,thumbPoints['N'][0].y - patternValues['Text Offset']],color='black')
            
            # Pattern name
            if langId == 0:
                name = hand + ' Thumb Reverse'
            else:
                if hand == "Left":
                    name = "Envers pouce gauche"
                else :
                    name = "Envers pouce droit"
            ax.text(thumbPoints['P'][0].x + 0.5 * (thumbPoints['O'][0].x - thumbPoints['P'][0].x), thumbPoints['P'][0].y + (handValues['Thumb Length']/2),name,{'ha': 'center', 'va': 'center'})
            
            if langId == 0 :
                text = "-Trace the pattern on the fabric reverse\n-Add a seam allowance as it isn\'t included\n-Sew DC on EB\n        QL on NM\n        PQ on JI\n        NO on IH\n"
            else :
                text = "-Tracer le patron sur l'envers du tissu\n-Ajouter les marges de coutures \n (non incluses dans le patron)\n-Coudre DC sur EB\n             QL sur NM\n             PQ sur JI\n             NO sur IH\n"
            ax.text(0.5,figH - 0.5, text,{'ha': 'left', 'va': 'top'})  

            # Thumb sheath
            ax.plot([TS.x, TS.x + TSLength,TS.x + TSLength,TS.x, TS.x],[TS.y,TS.y,TS.y - 2.0,TS.y - 2.0, TS.y],color='black')
            if langId == 0:
                label = "Thumb Sheath"
            else :
                label = "Gaine du pouce"
            ax.text(TS.x + (TSLength /2),TS.y - 1,label,{'ha': 'center', 'va': 'center'})
              
        return (returnCode,plt, fig)
    else:
        if langId == 0:
            returnCode = "Le patron excède le format A3, générer main et pouce indépendamment"
        else :
            returnCode = "Patterns exceed A3, draw hand and thumb independantly"
        return (returnCode, None,None)

if __name__ == '__main__':

    handValues = { 'Hand Contour' : 23, 'Hand Back' : 10, 'Hand Side' : 7, 'Wrist Contour' : 18.8, 'Thumb Base' : 5, 'Thumb Proximal' : 4.5,
               'Wrist Back' : 8, 'Thumb Length' : 11, 'Thumb Contour' : 7.5, 'Hand Length': 7.5,
               'Thumb Interior' : 7.5 }
    patternValues = {'Text Offset' : 0.2, 'Velcros Width' : 2.0 }

    code, plt, fig = drawPattern(handValues, patternValues, "Right", True, True, True, 0)
    code, plt, fig2 = drawPattern(handValues, patternValues, "Left", True, True, True, 0)
    code, plt, fig3 = drawPatternV2(handValues, patternValues, "Right", True, True, True, 0)
    code, plt, fig4 = drawPatternV2(handValues, patternValues, "Left", True, True, True, 0)
    
    # save figure ( printing png file had better resolution, pdf was lighter and better on screen)
    plt.show()

    
    # save figure ( printing png file had better resolution, pdf was lighter and better on screen)
   
    #fig.savefig('ExofingerA3Test.png', dpi=96)
    #fig.savefig('ExofingerA3Test.pdf')


