_English below_

**FR** |

# Exofinger - Générateur automatique de patron de gant 
Ce programme basé sur du python permet de générer automatiquement un patron de gant pour le dispositif Exofinger

![patterngenerator](./figs/patterngenerator.JPG)

## Prérequis

Ce programme nécessite d'avoir Python (>= 3.7) avec les modules matplotlib et bezier installés


## Installation

### Windows

- Windows Store > installer python 3
- Télécharger le dossier pattern_generator
- Ouvrir un invite de commandes et taper les commandes suivantes

  
```
pip3 install --user -r requirements.txt
cd Downloads/pattern_generator
python3 ExofingerGlovePatternGenerator.py [file.json]

L'argument optionnel file.json permet d'initialiser l'interface avec des valeurs particulières sauvegardés auparavant. 

Le fichier .json peut être créé en sauvegardant les paramètres que vous aurez modifiés dans l'application.
```

### Ubuntu >= 20.04

```
sudo apt-get install python3 python3-pip
git clone git@gitlab.inria.fr:humanlab-inria/exofinger.git
cd exofinger/pattern_generator
pip3 install --user -r requirements.txtpython3 ExofingerGlovePatternGenerator.py [file.json]

L'argument optionnel file.json permet d'initialiser l'interface avec des valeurs particulières sauvegardés auparavant. 

Le fichier .json peut être créé en sauvegardant les paramètres que vous aurez modifiés dans l'application.
```
## Organisation des dossiers

Ce dossier contient :

* Le code source
  * ExofingerGlovePatternGenerator.py
  * ExofingerGlovePattern.py
* Les images :
  * en.png
  * fr.png
  * HLIDark.png
  * HowToMeasure.png
  * HowToMeasureButton.png

## Licence

Le code source est sous licence [Cecill](../COPYING) 


**EN** |

# Exofinger Glove Pattern Generator
This program based on python allows to generate made to measure patterns to make gloves for exofinger systems.

![patterngenerator](./figs/patterngenerator_en.JPG)

## Requirements

It requires Python >= 3.7 with matplotlib and the bezier modules.

## Installation

### Windows

- Windows Store > install python 3
- Download the folder pattern_generator
- Open a command prompt and write the following commands : 

  
```
pip3 install --user -r requirements.txt
cd Downloads/pattern_generator
python3 ExofingerGlovePatternGenerator.py [file.json]

The optional argument file.json allows to initialise the interface with specific values
for most of the parameters. If not provided, default values are set.

```

### Ubuntu >= 20.04

```
sudo apt-get install python3 python3-pip
git clone git@gitlab.inria.fr:humanlab-inria/exofinger.git
cd exofinger/pattern_generator
pip3 install --user -r requirements.txt
python3 ExofingerGlovePatternGenerator.py [file.json]

The optional argument file.json allows to initialise the interface with specific values
for most of the parameters. If not provided, default values are set.

```

## Folder pattern_generator

This folder contains :

* source code:
  * ExofingerGlovePatternGenerator.py
  * ExofingerGlovePattern.py
* images resources:
  * en.png
  * fr.png
* Des exemples de fichier json
  * input/*.json

## License

The code is under [Cecill](../COPYING) license