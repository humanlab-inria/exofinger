_English below_

**FR** |

# Exofinger V2.1

![exofinger_pcb.png](./images/exofinger_pcb.png)

## Introduction

Exofinger V2.1 est une version améliorée de [Exofinger V1](https://wikilab.myhumankit.org/index.php?title=Projets:Exofinger_:_Thumb), prototype réalisé lors du [Fabrikarium](https://www.ariane.group/fr/actualites/bienvenue-au-fabrikarium-2020/) du 20 au 22 octobre 2020.

Ce prototype est une aide à la pince de la main gauche en rapprochant le pouce du reste des doigts chez une personne qui peut mobiliser son pouce. Cette orthèse de pouce motorisée doit permettre la préhension d'un objet.

Les améliorations amenées sont mécanique pour augmenter la course de serrage du pouce et rendre le prototype plus utilisable au jour le jour. Pour cela, les principales modifications sont :

* Passer d'un moteur linéaire à un moteur rotatif pour augmenter la course du fil de serrage par l'intermédiaire d'une poulie.
* Développer un module de bouton sans-fil à base de communication Bluetooth Low Energy (ble)
* Utiliser des [AdaFruit feather nRF52840 express](https://www.adafruit.com/product/4062) qui permettent:
  * la communication ble
  * la recharge des batteries depuis leur connecteur USB
* Réduire la taille du boitier
  
![prototype](./figures/exomotor_avec_gant.jpg)

## Les modules

Le prototype est désormais constitué de deux modules qui communiquent sans-fil par ble. Un module (Exomotor) est en charge de contrôler le moteur qui actionne le pouce via le fil en nylon et un autre module (Exobutton) qui gère le bouton pour envoyer l'ordre de serrage/déserrage.

Cette architecture distribuée entre actionneur et déclencheur, pourra être utilisé pour d'autres prototypes.

### Exomotor

![Exomotor](./figures/exomotor.jpg)
#### Electronique

L'Exomotor est architecturé autour d'une carte [AdaFruit feather nRF52840 express](https://www.adafruit.com/product/4062). La carte est alimentée par une batterie [LiPo 1S1P 3.7V 500mAh ICP303450PA](https://www.conrad.fr/p/accu-lithium-polymere-37v-510mah-renata-icp303450pa-1214020). Un [moto-réducteur CC 6V 1:200 avec encodeur intégré](https://fr.aliexpress.com/item/4000381882503.html?spm=a2g0s.9042311.0.0.1ec66c375r4WmL) est commandé par un [driver de moteur DRV8838](https://www.pololu.com/product/2990).

#### Mécanique

Le [moto-réducteur CC 6V 1:200 avec encodeur intégré](https://fr.aliexpress.com/item/4000381882503.html?spm=a2g0s.9042311.0.0.1ec66c375r4WmL) est couplé avec une bobine en V pour enrouler un câble. Celui-ci passe dans une gaine en téflon entre le boitier moteur et le gant, puis dans une autre gaine thermoformée dans le gant, enfin son extrémité est fixée sur le gant au niveau de la phalange distale du pouce.

 L’enroulement de ce câble provoque une force de tension qui entraîne une fermeture du pouce sur l’index. Ainsi l’Exofinger constitue une orthèse motorisée « jointless » c’est-à-dire sans articulation.

Le câble est un fil de pêche standard en Nylon de section 0.6mm – dimensionné pour une « force de tension » de 20kg.

#### Logiciel

Le module est configuré pour être un serveur BLE de type HID et se connecter à Exobutton. Il déclenchera l'action lorsqu'il recevra le caractère 'A' de Exobutton.

Il est également configuré en client BLE de type uart. Cela lui permet de se connecter à une App Android pour lui permettre de changer sa configuration.

Un contrôleur PID permet de contrôler la position du moteur qui actionne le pouce.

### Exobutton

![Exobutton](./figures/exobutton.jpg)
#### Electronique

Le bouton est architecturé autour d'une carte [AdaFruit feather nRF52840 express](https://www.adafruit.com/product/4062). La carte est alimentée par une batterie [LiPo 1S1P 3.7V 500mAh ICP303450PA](https://www.conrad.fr/p/accu-lithium-polymere-37v-510mah-renata-icp303450pa-1214020). Trois [micro-switches](https://www.conrad.fr/p/microrupteur-hartmann-microhart-mdb1-05c01c03a-125-vac-3-a-1-x-onon-a-rappel-1-pcs-704738) montés en parallèle sur entre l'entrée A0 et la masse complètent le schéma électronique ultra-simple du bouton.

#### Mécanique

Le bouton est constitué d'un boîtier cylindrique en trois parties. Le fond du boîtier accueille la batterie et la carte électronique qui sont superposée. La partie supérieure (immobile) du boîtier supporte les trois micro-switches et enfin la partie mobile du bouton est appuyée sur les microswitches et quatre ressorts optionnels et maintenue en place par la partie supérieur du boîtier.

Le port micro-USB de la carte électronique est laissé accessible par une ouverture sur la partie supérieure du boîtier.

Les deux parties immobiles du bouton sont maintenues entre elles par trois vis.

Les quatres ressorts servent de variable d'ajustement sur la sensibilité / dureté du bouton. Il ne sont mécaniquement pas nécessaires. Les ressorts des micro-switches sont suffisants pour soutenir le bouton (pas de déclenchement à vide), dans ce cas la sensibilité est maximale, mais il ne faut pas avoir la main trop lourde lors de l'activation car les micro-switches prennent les chocs en direct. A l'inverse ajouter quatre ressorts de raideur importante permet de déclencher le bouton par une frappe plus lourde sans risquer d'endommager les micro-switches.

##### Vue du boîtier ouvert

![Boîtier du bouton ouvert](./figures/button2.png)

##### Vue du boîtier ouvert avec la partie mobile du bouton

![Boîtier du bouton sans le capot](./figures/button1.png)

##### Vue du boîtier fermé

![Boîtier du bouton fermé](./figures/button.png)

#### Logiciel

Le bouton est configuré pour être un périphérique BLE de type HID. Lorsqu'il sera connecté à l'Exomotor, il sera vu comme un clavier et chaque pression sur le bouton simulera l'envoi d'un caractère 'A' sur le lien BLE.

Lorsque l'un des micro-switch est fermé, la pin A0 est connectée à la masse et à l'inverse lorsqu'ils sont tous ouverts, la pin A0 est laissée libre. Il faut donc la configurer avec une résistance de pull-up interne pour que la lecture de l'entrée A0 soit à 1 si l'entrée n'est pas connectée à la masse.

Pour le moment, la lecture de l'entrée A0 se fait dans la boucle principale du programme (fonction `loop()`), il pourra être intéressant pour limiter la consommation d'énergie (et ainsi augmenter la durée de vie du bouton sur batterie) de détecter les appuis par interruption (il faudra dans ce cas faire bien attention à traiter correctement les rebonds sur les micro-switches)

## Analyse de risque

* Risques d'inflammation liés aux batteries: [LiPo 1S1P 3.7V 500mAh ICP303450PA](https://www.conrad.fr/p/accu-lithium-polymere-37v-510mah-renata-icp303450pa-1214020). La
[fiche technique](https://asset.conrad.com/media10/add/160267/c1/-/en/001214020DS01/fiche-technique-1214020-accu-lithium-polymere-37v-510mah-renata-icp303450pa.pdf) montre qu'elle dispose d'un circuit de protection et d'une large fourchette de températures de stockage : -20 à +45 degrés. La qualité des batteries limite les risques de détérioration et d'inflammation.
* Risques liés à un défaut de commande moteur: Le [moto-réducteur CC 6V 1:200 avec encodeur intégré](https://fr.aliexpress.com/item/4000381882503.html?spm=a2g0s.9042311.0.0.1ec66c375r4WmL) peut produire au maximum un couple de 2.5kg.cm. Dans le cas le plus défavorable, c’est-à-dire quand le bras de levier entre le couple moteur et la tension du câble est le plus faible, ie quand le câble est le moins enroulé : Tensionmax = couplemax/diamètreBobinemin (sans prendre en compte les forces de frottements qui ne sont pas négligeables ~20-30%) **A COMPLETER** Par ailleurs, le système de transmission n'est pas réversible (câble) il n'y a pas de risque de provoquer un mouvement dans une autre direction ou dans un autre sens.
* Pour un usage prolongé, il existe des risques de TMS lié au mauvais alignement entre l’axe de rotation biologique et celui de l’orthèse. L’Exofinger étant une orthèse sans articulation, il n’y a pas ce risque.
* Le câble parcourt presque tout le tour de la main, une fois en tension celui-ci pourrait entrainer une pression sur la main. C’est pourquoi la gaine dans le gant est semi-rigide et présente un diamètre de **A COMPLETER **qui permet d’absorber et de distribuer la pression du câble sur la main.
* Risque électrique : Les tensions ne dépassant pas 3.7 Volts, il n’y a aucun risque d’électrisation (risque à partir de 20V dans un environnement très humide).
* Risque de prise de contrôle par un tiers : La commande ne répond qu’avec des appareils pairés

## Licences

* Le logiciel est sous licence [Cecill](./COPYING)
* Les boitiers électroniques et mécaniques sont sous licence [Creative-Common](./materiel/COPYING.md)

**EN** |

# Exofinger V2.1

## Introduction


Exofinger V2.1 is an improved version of [Exofinger V1](https://wikilab.myhumankit.org/index.php?title=Projets:Exofinger_:_Thumb), prototype made during the [Fabrikarium](https://www.ariane.group/fr/actualites/bienvenue-au-fabrikarium-2020/) from 20 to 22 October 2020.

This prototype is an aid for pinch movement of the left hand by bringing the thumb closer to the rest of the fingers for person who can not mobilize his thumb. This motorized thumb orthosis must allow the gripping of an object.

The improvements made are mechanical to increase the thumb-tightening stroke and make the prototype more usable on a day-to-day basis. For this, the main changes are:

* Switch from a linear motor to a rotary motor to increase the cable stroke via a pulley.
* Develop a wireless button module based on Bluetooth Low Energy communication (ble) 
* Use [AdaFruit feather nRF52840 express](https://www.adafruit.com/product/4062) that allow: 
    * BLE communication 
    * Charging batteries from their USB connector 
* Reduce the size of the casing

![prototype](./figures/exomotor_avec_gant.jpg)

## Modules 

The prototype now consists of two modules that communicate wirelessly by BLE. One module (Exomotor) is in charge of controlling the motor that operates the thumb via the nylon wire and another module (Exobutton) that manages the button to send the tightening/loosening order. This architecture, distributed between actuator and trigger, can be used for other prototypes.

### Exomotor

![Exomotor](./figures/exomotor.jpg)

#### Electronics

The Exomotor is built around a board [AdaFruit feather nRF52840 express](https://www.adafruit.com/product/4062). The board is powered by a battery [LiPo 1S1P 3.7V 500mAh ICP303450PA](https://www.conrad.fr/p/accu-lithium-polymer-37v-510mah-renata-icp303450pa-1214020). A [6V 1:200 DC motor reducer with integrated encoder](https://fr.aliexpress.com/item/4000381882503.html?spm=a2g0s.9042311.0.0.1ec66c375r4WmL) is controlled by a [DRV8838 engine driver](https://www.pololu.com/product/2990).

#### Mechanics 

The [6V 1:200 DC motor reducer with integrated encoder](https://fr.aliexpress.com/item/4000381882503.html?spm=a2g0s.9042311.0.0.1ec66c375r4WmL) is coupled with a V-coil to wind the cable. The latter passes in a Teflon sheath between the Exomotor and the glove and then in another thermoformed sheath in the glove. Finally its end is fixed on the glove at the level of the distal phalanx of the thumb. The winding of the cable causes a tension force that causes the thumb to close on the index finger. Thus, the Exofinger constitutes a motorized orthosis "jointless" in other words without articulation. 

The cable is a standard Nylon fishing line of 0.6mm section – sized for a "tension force" of 20kg.

#### Software 

The module is configured to be a HID type BLE server and connect to Exobutton. It will trigger the action when it will receive the 'A' character from Exobutton. 

It is also configured as a uart BLE client. This allows it to connect to an Android App to allow it to change its configuration. 

A PID controller allows to control the position of the motor that drives the thumb.

### Exobutton

![Exobutton](./figures/exobutton.jpg)

#### Electronics 

The button is build around a board [AdaFruit feather nRF52840 express](https://www.adafruit.com/product/4062). The board is powered by a battery [LiPo 1S1P 3.7V 500mAh ICP303450PA](https://www.conrad.fr/p/accu-lithium-polymer-37v-510mah-renata-icp303450pa-1214020). Three [micro-switches](https://www.conrad.fr/p/microrupteur-hartmann-microhart-mdb1-05c01c03a-125-vac-3-a-1-x-onon-a-rappel-1-pcs-704738) mounted in parallel between the A0 input and the ground complete the ultra-simple electronic diagram of the button.

#### Mechanics 

The button consists of a cylindrical housing in three parts. The back of the casing accommodates the battery and the electronic board which are superimposed. The upper (stationary) part of the casing supports the three micro-switches and finally the moving part of the button is pressed on the microswitches and four optional springs. This moving part is held in place by the upper part of the casing. 

The micro-USB port of the electronic board is left accessible through an opening on the upper part of the casing.

The two stationary parts of the button are held together by three screws. 

The four springs serve as an adjustment variable on the sensitivity/hardness of the button. They are mechanically not necessary. The springs of the micro-switches are sufficient to support the button (no vacuum triggering), in this case the sensitivity is maximum, but do not have too heavy a hand when activating because the micro-switches take shocks live. Conversely, adding four springs of significant stiffness makes it possible to trigger the button with a heavier strike without risking damage to the micro-switches.

##### Open casing view

![Open casing view of the button](./figures/button2.png)

##### View of the open casing with the moving part of the button

![View of the open casing with the moving part of the button](./figures/button1.png)

##### Closed casing view

![Closed casing view of the button](./figures/button.png)

#### Software 

The button is configured to be a HID type BLE device. When connected to the Exomotor, it will be seen as a keyboard and each press of the button will simulate sending an 'A' character to the BLE link.

When one of the micro-switches is closed, the A0 pin is connected to the ground and conversely when they are all open, the A0 pin is left free. It must therefore be configured with an internal pull-up resistor so that the reading of the A0 input is at 1 if the input is not connected to the ground.

For the moment, the reading of the A0 input is done in the main loop of the program ('loop()' function), it may be interesting to limit the energy consumption (and thus increase the life of the button on battery) to detect the press by interruption (it will be necessary in this case to properly treat the rebounds on the micro-switches)

## Licenses

* The software is under [Cecill](./COPYING) license
* Exofinger mechanical and electronic parts are under [Creative-Common](./materiel/COPYING.md) license
