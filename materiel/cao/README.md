**FR** |

## Organisation des dossiers

Vous trouverez les fichiers sources des assemblages si vous souhaitez modifier une pièce dans le dossier **freecad** ou **solidworks**.
Pour télécharger FreeCad rendez vous sur : https://www.freecadweb.org/downloads.php?lang=fr

Et vous trouverez les fichier en .STL ou .STEP pour l'impression 3D dans les dossiers **stl** ou **step** 

## Licence

Les plans mécaniques sont sous licence [Creative-Common](../COPYING.md)

**EN** |

## Folders structure

You can find the source files for assemblies, if you want to edit a part, in the **freecad** or **solidworks** folders. To download FreeCad go to: https://www.freecadweb.org/downloads.php?lang=en

You will find the files in . STL or STEP for 3D printing in the **stl** or **step** folders.

## License

Exofinger electronic schematics are under [Creative-Common](../COPYING.md) license