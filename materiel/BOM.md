_English below_

**FR** |

## Liste du matériel


À acheter :



| Désignation | Prix/Uni* | Nombre | Ref |
| ------ | ------ | ------ |------ | 
| Moteur |  9,24 € | 1 | [1:500](https://fr.aliexpress.com/item/4000381882503.html?spm=a2g0s.9042311.0.0.27426c37DCL5CL) |
| Driver moteur | 3,92 € HT | 1 | [Pololu](https://www.pololu.com/product/2990)|
| Batteries | 11,99€ TTC | 2 | [conrad](https://www.conrad.fr/p/accu-lithium-polymere-37v-510mah-renata-icp303450pa-1214020) | 
| cables alimentation batteries | 9€59 /20pcs| 2| [amazon](https://www.amazon.fr/Pi%C3%A8ces-2-Broches-Connecteur-Silicone-Femelle/dp/B07449V33P/ref=sr_1_9?__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=Connecteur+Jst+PH+2P&qid=1632384697&s=electronics&sr=1-9) | 
| Microswitches | 1.60€ TTC | 3 | [conrad](https://www.conrad.fr/p/microrupteur-hartmann-microhart-mdb1-05c01c03a-125-vac-3-a-1-x-onon-a-rappel-1-pcs-704738) | 
| Connecteur magnétique | 14€50 | 2 | [aliexpress](https://fr.aliexpress.com/item/32953067218.html?spm=a2g0s.9042311.0.0.59176c37wGvfLa) [amazon](https://www.amazon.fr/gp/product/B07JZNTWNP/ref=ox_sc_rp_title_rp_1?smid=&psc=1&pf_rd_p=93a81b7f-13c8-42a3-be8d-5f59cdb18b65&pd_rd_wg=iVSpr&pd_rd_i=B07JZNTWNP&pd_rd_w=VdUBm&pd_rd_r=fc885e06-7c99-4d93-94a0-1ed4056e38f2) | 
| Vis M2 x 8 mm | 6€/100pcs | 9  | [vis express](https://www.vis-express.fr/fr/micro-vis-autotaraudeuse-tete-fraisee-tf-phillips-acier/52604-336644-vis-autotaraudeuse-pour-plastique-tete-fraisee-tf-phillips-2x8-zingue-3663072257849.html#/21-conditionnement-unitaire) | 
| Vis M2 x 12 mm | 1,52€/100pcs | 8  | [conrad](https://www.conrad.fr/p/vis-a-tete-fraisee-toolcraft-194831-m212-d963-48a2k-100-pcs-m2-12-mm-tete-fraisee-plat-acier-na-521902) | 
| écrou | 3.24€/100pcs | 10 |[vis-express](https://www.vis-express.fr/fr/ecrou-hu-a2-din-934/36811-612666-ecrou-hexagonal-m2-inox-a2-wax-lubrifie-3663072166431.html) | 
| Gaine Nylon 4,5/6 mm | 22.88€/30m | 1 | Gaine souple de ces diamètre (intérieur/extérieur), on utilise un câble électrique vidé, à défaut prendre [RS](https://fr.rs-online.com/web/p/tuyaux-a-air/4150323) (trop raide) | 
| Gaine PTFE main| 0.35€/m | 1 | [PTFE](https://ptfetubeshop.com/fr/product/tuyau-en-ptfe-1mm-x-2mm/) | 
| Connecteur pneumatique | 6€/5 | 1 | [RS](https://fr.rs-online.com/web/p/raccords-pneumatiques/2993819)|
| Ressort | 4,70 €/10pcs| 4 | [RS](https://fr.rs-online.com/web/p/ressorts-de-compression/0821267) | 
|fil de pêche (0,6mm) | 16€44 | 1 |[Savage Gear](https://savage-gear.com/products/terminal-tackle/leader-materials/fluorocarbon/soft-fluorocarbon/20m-0-60mm-/-21-6kg-/-48lbs) [predateur-peche.fr](https://www.predateur-peche.fr/fr4/savage-gear-fluorocarbone-soft-fluoro-carbon/s/23158) 
| anneaux 30 mm| 10€14/20pcs TTC| 2 | [RS](https://fr.rs-online.com/web/p/accessoires-de-cles/866557/)|
|sangle velcro| 8€95 | 1 | [alltricks](https://www.alltricks.fr/F-32738-chambres-a-air/P-1114617-sangle_granite_design_rockband_orange)|
| Fermoir à bijoux |9,99€/20pcs| 1| [amazon](https://www.amazon.fr/SUPROX-fermoir-bracelet-bricolage-accessoires/dp/B07QMBXDY6/)|
|Interrupteur glissière|0,45 € |1|[digikey](https://www.digikey.fr/product-detail/fr/c-k/OS102011MS2QS1/CKN9542-ND/1981413)|

*Prix a titre indicatif au moment de notre achat. Ces prix peuvent être amenés à varier.


**Coût total matériel : environ 150 €**

 ## Liste des équipements

- Une imprimante 3D
- Une machine à coudre
- Un tournervis cruciforme pour vis M2 et hexagonal pour écrou M2
- Un fer à souder et de l'étain

**EN** |

## Material List


To buy :



| Designation | Price/Uni | Quantity | Ref |
| ------ | ------ | ------ |------ | 
| Motor |  9,24 € | 1 | [1:200](https://fr.aliexpress.com/item/4000381882503.html?spm=a2g0s.9042311.0.0.27426c37DCL5CL) |
| Motor driver | 3,92 € ex-tax | 1 | [Pololu](https://www.pololu.com/product/2990)|
| Battery | 11,99€ including tax | 2 | [conrad Renata](https://www.conrad.fr/p/accu-lithium-polymere-37v-510mah-renata-icp303450pa-1214020) | 
| cables alimentation batteries | 9€59 /20pcs| 2| [amazon](https://www.amazon.fr/Pi%C3%A8ces-2-Broches-Connecteur-Silicone-Femelle/dp/B07449V33P/ref=sr_1_9?__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=Connecteur+Jst+PH+2P&qid=1632384697&s=electronics&sr=1-9) | 
| Microswitch | 1.60€ including tax | 3 | [conrad Hartmann](https://www.conrad.fr/p/microrupteur-hartmann-microhart-mdb1-05c01c03a-125-vac-3-a-1-x-onon-a-rappel-1-pcs-704738) | 
| Magnetic connector | 14€50 | 2 | [aliexpress](https://fr.aliexpress.com/item/32953067218.html?spm=a2g0s.9042311.0.0.59176c37wGvfLa) [amazon](https://www.amazon.fr/gp/product/B07JZNTWNP/ref=ox_sc_rp_title_rp_1?smid=&psc=1&pf_rd_p=93a81b7f-13c8-42a3-be8d-5f59cdb18b65&pd_rd_wg=iVSpr&pd_rd_i=B07JZNTWNP&pd_rd_w=VdUBm&pd_rd_r=fc885e06-7c99-4d93-94a0-1ed4056e38f2) | 
| Screw M2 x 8 mm | 6€/100pcs | 9  | [vis express](https://www.vis-express.fr/fr/micro-vis-autotaraudeuse-tete-fraisee-tf-phillips-acier/52604-336644-vis-autotaraudeuse-pour-plastique-tete-fraisee-tf-phillips-2x8-zingue-3663072257849.html#/21-conditionnement-unitaire) | 
| screw M2 x 12 mm | 6€/100pcs | 8  | [vis express](https://www.vis-express.fr/fr/vmx-tf-pozi-a2-din-965-iso-7046/35275-940946-vis-metaux-inox-a2-tete-fraisee-pozi-2x12-3663072146754.html#/26-conditionnement-100_pieces) | 
| nut | 3.24€/100pcs | 10 |[vis-express](https://www.vis-express.fr/fr/ecrou-hu-a2-din-934/36811-612666-ecrou-hexagonal-m2-inox-a2-wax-lubrifie-3663072166431.html) | 
| nylon sheath | 22.88€/30m | 1 | Flexible sheath with these diameters (inside/outside), we use an emptied electric cable, otherwise take [RS](https://fr.rs-online.com/web/p/tuyaux-a-air/4834936/) | 
| PTFE sheath | 0.35€/m | 1 | [PTFE](https://ptfetubeshop.com/fr/product/tuyau-en-ptfe-1mm-x-2mm/) | 
| pneumatic connector | 6€/5 | 1 | [RS](https://fr.rs-online.com/web/p/raccords-filetes-droits/6862649/)|
| Spring | 4,70 €/10pcs| 4 | [RS](https://fr.rs-online.com/web/p/ressorts-de-compression/0821267) | 
|fishing line (0,6mm) | 16€44 | 1 |[Savage Gear](https://savage-gear.com/products/terminal-tackle/leader-materials/fluorocarbon/soft-fluorocarbon/20m-0-60mm-/-21-6kg-/-48lbs) [predateur-peche.fr](https://www.predateur-peche.fr/fr4/savage-gear-fluorocarbone-soft-fluoro-carbon/s/23158) 
| rings 30 mm| 10€14/20pcs including tax| 2 | [RS](https://fr.rs-online.com/web/p/accessoires-de-cles/866557/)|
|velcro| 6€41 | 1 | [amazon](https://www.amazon.fr/dp/B00APYEQVK?psc=1&smid=A1X6FK5RDHNB96&ref_=chk_typ_imgToDp)|

**Total cost : around 150 €**

 ## Equipments list

- A 3D printer
- A sewing machine
- A cruciform screwdriver for M2 screw and hexagonal for M2 nut
- A soldering iron and tin

