 [Boitiers exofinger](https://gitlab.inria.fr/humanlab-inria/exofinger/-/tree/master/materiel), copyright 2021 by [Christophe Braillon](https://gitlab.inria.fr/braillon) / [Humanlab Inria](https://www.inria.fr) is licensed under  Attribution-NonCommercial-ShareAlike 4.0 International [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![logo cc by-nc-sa](./cc_by-nc-sa.png)

