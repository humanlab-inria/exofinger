
![exofinger_pcb.png](./images/exofinger_pcb.png)

**FR** |

## Organisation des dossiers

Vous trouverez les fichiers du dossier de fabrication des cartes électroniques au format **Kicad**.

Pour télécharger Kicad rendez vous sur : <https://www.kicad.org/download/>

## Licence

Le schéma et le dossier de fabrication électronique sont sous licence [Creative-Common](../COPYING.md)electronic production file

**EN** |

## Folders structure

Vous trouverez les fichiers du dossier de fabrication des cartes électroniques au format **Kicad**.

You can find the source files for electronic board in **Kicad** format.

To download Kicad go to: <https://www.kicad.org/download/>

## License

Exofinger Electronic scehmatic & production files are under [Creative-Common](../COPYING.md) license